return {
	multipass: true,
	plugins: [
		{
			name: "cleanupAttrs",
		},
		{
			name: "cleanupEnableBackground",
		},
		{
			name: "cleanupIDs",
		},
		{
			name: "cleanupListOfValues",
		},
		{
			name: "cleanupNumericValues",
			params: {
				floatPrecision: 2,
			},
		},
		{
			name: "collapseGroups",
		},
		{
			name: "convertColors",
		},
		{
			name: "convertPathData",
		},
		{
			name: "convertShapeToPath",
		},
		{
			name: "convertStyleToAttrs",
		},
		{
			name: "convertTransform",
		},
		{
			name: "mergePaths",
		},
		{
			name: "minifyStyles",
		},
		{
			name: "moveElemsAttrsToGroup",
		},
		{
			name: "moveGroupAttrsToElems",
		},
		{
			name: "removeAttrs",
		},
		{
			name: "removeComments",
		},
		{
			name: "removeDesc",
		},
		{
			name: "removeDimensions",
		},
		{
			name: "removeDoctype",
		},
		{
			name: "removeEditorsNSData",
		},
		{
			name: "removeElementsByAttr",
		},
		{
			name: "removeEmptyAttrs",
		},
		{
			name: "removeEmptyContainers",
		},
		{
			name: "removeEmptyText",
		},
		{
			name: "removeHiddenElems",
		},
		{
			name: "removeMetadata",
		},
		{
			name: "removeNonInheritableGroupAttrs",
		},
		{
			name: "removeRasterImages",
		},
		{
			name: "removeStyleElement",
		},
		{
			name: "removeTitle",
		},
		{
			name: "removeUnknownsAndDefaults",
		},
		{
			name: "removeUnusedNS",
		},
		{
			name: "removeUselessDefs",
		},
		{
			name: "removeUselessStrokeAndFill",
		},
		{
			name: "removeViewBox",
		},
		{
			name: "removeXMLProcInst",
		},
		{
			name: "sortAttrs",
		},
		{
			name: "transformsWithOnePath",
		},
	],
	js2svg: {
		pretty: true,
		indent: "\t",
	},
}
