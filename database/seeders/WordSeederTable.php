<?php

namespace Database\Seeders;

use App\Models\Type;
use App\Models\Word;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WordSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = Type::get();
        $words = [
            [
                'name' => 'DA',
                'type_id'=>$type[0]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'BA',
                'type_id'=>$type[1]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'NADA',
                'type_id'=>$type[0]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'BALE',
                'type_id'=>$type[1]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'DANÇA',
                'type_id'=>$type[0]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'BATOM',
                'type_id'=>$type[1]->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];

        foreach ($words as $word)
            Word::create($word);
    }
}
