<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;

class PermissionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$permission_ids = []; // an empty array of stored permission IDs
		// iterate though all routes
		foreach (Route::getRoutes()->getRoutes() as $key => $route) {
			$name = $route->getName() ?? null;
			$action = $route->getActionname();
			$_action = preg_split('/@/',$action);
			$controller = $_action[0];
			$method = end($_action);

			$permission_check = Permission::where(
				['controller'=>$controller,'method'=>$method]
			)->first();
			if(!$permission_check && !preg_match('/(Auth|Closure)/',$controller)){
				$permission = new Permission;
				$permission->name = $name;
				$permission->controller = $controller;
				$permission->method = $method;
				$permission->save();
				// add stored permission id in array
				$permission_ids[] = $permission->id;
			}
		}
		// find admin role.
		$admin_role = Role::first();
		// atache all permissions to admin role
		$admin_role->permissions()->sync($permission_ids);
	}
}
