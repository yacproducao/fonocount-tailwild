<?php

namespace Database\Seeders;

use App\Models\Concordance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConcordanceSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $concordances = [
            [
                'name' => 'Congruências'
            ],
            [
                'name' => 'Incongruências'
            ],
        ];
        foreach ($concordances as $concordance){
            $concordan = new Concordance($concordance);
            $concordan->save();
        }
    }
}
