<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$roles = [
			[
				'name' => 'Administrador',
				'created_at' => now(),
				'updated_at' => now()
			],
			[
				'name' => 'Paciente',
				'created_at' => now(),
				'updated_at' => now()
			]
		];
		foreach ($roles as $role) {
			Role::create($role);
		}
	}
}
