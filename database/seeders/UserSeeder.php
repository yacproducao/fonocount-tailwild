<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Person;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
	protected $users;
	protected $role;

	public function __construct()
	{
		$this->role = Role::first();
		$this->users =
			[
				[
					'name' => 'Ana',
					'user' => 'ana',
					'email' => 'luiza.faria@unesp.br',
					'email_verified_at' => now(),
					'password' => Hash::make('secret'),
					'created_at' => now(),
					'updated_at' => now()
				]
			];
	}

	public function run()
	{
		if (env('APP_ENV') != 'production')
			$this->development();
		else
			$this->production();
	}

	private function production()
	{
		foreach ($this->users as $key => $user) {
			$usuario = User::factory()
				->state($user)
				->create();
			$usuario->role()->associate($this->role);
			$usuario->save();
		}
	}

	private function development()
	{
		foreach ($this->users as $key => $user) {
			$usuario = User::factory()
				->state($user)
				->create();
			$usuario->role()->associate($this->role);
			$usuario->save();
		}
	}
}
