<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\Timezone;
use App\Models\Translation;
use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
	protected $countries;

	public function __construct()
	{
		if (file_exists(resource_path('json') . '/countries_states_cities.json')) {
			$this->countries = json_decode(file_get_contents(resource_path('json') . '/countries_states_cities.json'), true);
		} else {
			$this->countries = [];
		}
	}

	public function run()
	{
		foreach ($this->countries as $country) {
			if ($country['name'] == "Brazil") {
				$cntry = Country::create(
					Arr::add(
						Arr::only($country, ['name', 'native', 'iso2', 'iso3', 'phone_code', 'latitude', 'longitude']),
						'emojis',
						[
							'emoji' => $country['emoji'],
							'emojiU' => $country['emojiU']
						]
					)
				);
				if (is_array($country['timezones']))
					foreach ($country['timezones'] as $timezone) {
						if ($tz = Timezone::where('zoneName', $timezone['zoneName'])->first()) {
							$cntry->timezones()->sync([$tz->id], true);
						} else {
							$tz = Timezone::create($timezone);
							$cntry->timezones()->sync([$tz->id], true);
						}
					}
				if (is_array($country['translations']))
					foreach ($country['translations'] as $lang => $translation) {
						$cntry->translations()->save(new Translation([
							'lang_code' => $lang,
							'translation' => $translation,
						]));
					}
				foreach ($country['states'] as $state) {
					$stte = new State(Arr::add(Arr::only($state, ['name']), 'uf', $state['state_code']));
					$stte->country()->associate($cntry);
					$stte->save();
					foreach ($state['cities'] as $city) {
						$cty = new City(Arr::only($city, ['name', 'latitude', 'longitude']));
						$cty->state()->associate($stte);
						$cty->is_capital = $country['capital'] == $city['name'];

						$cty->save();
					}
				}
			}
		}
	}
}
