<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('name');
			$table->string('description')->nullable()->default(null);
			$table->timestamps();
		});
		Schema::table('users', function (Blueprint $table) {
			$table->foreignUuid('role_id')->nullable()->default(null)->constrained()->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('roles');
		Schema::table('users', function (Blueprint $table) {
			$table->dropForeign('user_role');
			$table->dropColumn('role_id');
		});
		Schema::enableForeignKeyConstraints();
	}
}
