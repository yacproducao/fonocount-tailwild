<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->uuidMorphs('addressable');
			$table->foreignUuid('city_id')->nullable()->default(null)->constrained()->onUpdate('set null')->onDelete('cascade');
			$table->foreignUuid('state_id')->constrained()->onDelete('cascade');
			$table->string('zip_code',64)->nullable()->default(null);
			$table->string('street')->nullable()->default(null);
			$table->string('district')->nullable()->default(null);
			$table->string('number')->nullable()->default(null);
			$table->string('extra')->nullable()->default(null);
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('addresses');
		Schema::enableForeignKeyConstraints();
	}
}
