<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->uuid('state_id');
			$table->string('name');
			$table->decimal('latitude', 14, 6)->nullable()->default(null);
			$table->decimal('longitude', 14, 6)->nullable()->default(null);
			$table->boolean('is_capital')->default(false);

			$table->foreign('state_id', 'state_constraint')->references('id')->on('states')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('cities');
		Schema::enableForeignKeyConstraints();
	}
}
