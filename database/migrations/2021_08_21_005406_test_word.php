<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestWord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_word', function (Blueprint $table) {
            $table->foreignUuid('test_id')->constrained();
            $table->foreignUuid('word_id')->constrained();
            $table->boolean('response')->default(0);
            $table->time('time')->default('00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_word');
    }
}
