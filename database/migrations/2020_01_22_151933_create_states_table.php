<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('states', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->uuid('country_id');
			$table->string('name');
			$table->string('uf');
			$table->boolean('is_capital')->default(false);

			$table->foreign('country_id', 'country_constraint')->references('id')->on('countries')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::dropIfExists('states');
		Schema::enableForeignKeyConstraints();
	}
}
