<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimezoneablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timezoneables', function (Blueprint $table) {
			$table->uuid('timezone_id');
			$table->uuidMorphs('timezoneable');

			$table->foreign('timezone_id', 'timezone_constraint')->references('id')->on('timezones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timezoneables');
    }
}
