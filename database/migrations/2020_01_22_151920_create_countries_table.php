<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('name');
			$table->string('native')->nullable()->default(null);
			$table->string('iso2', 16);
			$table->string('iso3', 16);
			$table->string('phone_code', 16);
			$table->decimal('latitude', 14,6)->nullable()->default(null);
			$table->decimal('longitude', 14,6)->nullable()->default(null);
			$table->json('emojis')->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('countries');
	}
}
