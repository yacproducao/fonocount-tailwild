<div class="absolute block inset-x-0 bottom-0 p-8 font-light text-sm text-center">
	© {{ date('Y') }} Desenvolvido por <x-animated-logo />
</div>
