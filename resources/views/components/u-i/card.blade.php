<div {{ $attributes->merge(['class'=>'bg-white bg-opacity-75 backdrop-filter backdrop-blur-sm dark:bg-gray-800 overflow-hidden shadow flex flex-col items-center sm:rounded-lg']) }}>
    {{ $slot }}
</div>