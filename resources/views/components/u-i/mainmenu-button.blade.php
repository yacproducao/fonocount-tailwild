<a href="{{ $url }}" class="{{ join(' ', $classes) }}">
    <i class="{{ $icon }}"></i>
</a>
