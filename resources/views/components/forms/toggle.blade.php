<div class="flex justify-start flex-grow">
	<label for="{{ $id }}" class="flex flex-row flex-grow">
		@if(isset($caption))
		<div class="{{ join(' ', $labelClasses) }}">{{ $caption }}</div>
		@endif
		<div class="relative cursor-pointer">
			<input id="{{ $id }}" type="checkbox" class="hidden" {!! $name ? "name=\"$name\"" : '' !!} {{ $state ? 'checked' : '' }}/>
			<div class="toggle-path bg-gray-200 w-9 h-5 rounded-full shadow-inner"></div>
			<div class="toggle-circle absolute w-3.5 h-3.5 bg-white dark:bg-black dark:bg-opacity-80 rounded-full shadow inset-y-0 right-0"></div>
		</div>
	</label>
</div>
