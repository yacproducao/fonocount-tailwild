<div class="mainmenu">
	<a href="{{ route('home') }}" class="link{{ request()->is(['admin', 'admin/']) ? ' active' : '' }}">
		<x-u-i.symbol width="48" classes="fill-current" />
	</a>
</div>
