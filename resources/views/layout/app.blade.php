<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="min-h-screen">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>{{ (isset($title)?$title." - ":"").env('APP_NAME')}}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="author" content="Fonocount">
	<meta name="theme-color" content="#37cecd">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Weather PWA">
	<link rel="apple-touch-icon" href="/images/icons/icon-152x152.png">
	<link href="{{ asset('/res/images/brand/icone.png') }}" rel="icon" type="image/png">
	<link type="text/css" href="{{mix('/css/layout.css')}}" rel="stylesheet">
	<link type="text/css" href="{{mix('/css/app.css')}}" rel="stylesheet">
	<script src="{{mix('/js/alpinecomponent.js')}}"></script>
	<script src="{{mix('/js/alpine.js')}}"></script>
	<link rel="manifest" href="/manifest.json">
	@stack('head')
</head>

<body class="{{ isset($classes) && is_array($classes) ? join(' ', array_unique( array_merge(['relative', 'min-h-screen', 'antialiased', 'sm:subpixel-antialiased'], $classes)) ) : join(' ', ['relative', 'min-h-screen', 'antialiased', 'sm:subpixel-antialiased'])}}">

	<div x-data="setup()" x-init="$refs.loading.classList.add('hidden'); setColors(color);" :class="{ 'dark': isDark}">
		<div x-ref="loading" class="fixed inset-0 z-50 flex items-center justify-center text-2xl font-semibold text-white bg-primary-darker">
			Carregando.....
		</div>
		<div class="flex flex-col h-screen justify-center antialiased items-center text-gray-900 {{ isset($classes) && is_array($classes) ? join(' ', array_unique($classes)):''}} dark:text-light px-7 py-20">

			@yield('content')
			<div class="fixed bottom-5 left-5">
				<button aria-hidden="true" @click="toggleTheme" class="p-2 transition-colors duration-200 rounded-full shadow-md bg-primary hover:bg-primary-darker focus:outline-none focus:ring focus:ring-primary">
					<svg x-show="isDark" class="w-8 h-8 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
					</svg>
					<svg x-show="!isDark" class="w-8 h-8 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" />
					</svg>
				</button>
			</div>
		</div>


	</div>
	<script src="{{ mix('js/manifest.js') }}"></script>
	<script src="{{ mix('js/vendor.js') }}"></script>
	<script src="{{ mix('js/app.js') }}"></script>
	<script src="{{ mix('js/script.js') }}"></script>

	<script>
		const setup = () => {
			const getTheme = () => {
				if (window.localStorage.getItem('dark')) {
					return JSON.parse(window.localStorage.getItem('dark'))
				}
				return !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
			}

			const setTheme = (value) => {
				window.localStorage.setItem('dark', value)
			}

			const getColor = () => {
				if (window.localStorage.getItem('color')) {
					return window.localStorage.getItem('color')
				}
				return 'teal'
			}

			const setColors = (color) => {
				const root = document.documentElement
				root.style.setProperty('--color-primary', `var(--color-${color})`)
				root.style.setProperty('--color-primary-50', `var(--color-${color}-50)`)
				root.style.setProperty('--color-primary-100', `var(--color-${color}-100)`)
				root.style.setProperty('--color-primary-light', `var(--color-${color}-light)`)
				root.style.setProperty('--color-primary-lighter', `var(--color-${color}-lighter)`)
				root.style.setProperty('--color-primary-dark', `var(--color-${color}-dark)`)
				root.style.setProperty('--color-primary-darker', `var(--color-${color}-darker)`)
				this.selectedColor = color
				window.localStorage.setItem('color', color)
			}

			return {
				loading: true,
				isDark: getTheme(),
				color: getColor(),
				toggleTheme() {
					this.isDark = !this.isDark
					setTheme(this.isDark)
				},
				setColors,
			}
		}
	</script>
	@stack('js')
</body>

</html>