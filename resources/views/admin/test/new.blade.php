@extends('admin.layout.app',[
'title'=>'Inicie o Teste',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])

@section('content')
<form action="">
    <div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">

        <div class="mt-8 bg-gray-300 rounded-md dark:bg-image-dark bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="grid grid-cols-1">
                <label for="filterPatient" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2" htmlfor="grid-role">
                    Escolha um Paciente
                </label>
                <select id="filterPatient" name="patient_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" data-endpoint="api/patient" data-labelfield="name" data-valuefield="id" data-current="{{ old('patient_id') }}" data-targetid="" data-updateevent="" required>
                    <option value="">Selecione um Paciente</option>
                </select>
            </div>
        </div>
        <div class="mt-8 bg-gray-300 rounded-md dark:bg-image-dark bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div>
                    <button type="button" id="congruenciaId" class="btn p-6 w-full h-full bg-blue-500 hover:bg-blue-200 text-white hover:text-blue-500">
                        <i class="fad fa-handshake fa-2x"></i> <span class="ml-4 text-lg leading-7 font-semibold leading-none tracking-wider">Congruência</span>
                    </button>
                </div>

                <div class="border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                    <button  type="button" id="incongruenciaId" class="btn p-6 w-full h-full bg-red-500 hover:bg-red-200 text-white hover:text-red-500">
                        <i class="fad fa-handshake fa-2x"></i>
                        <span class="ml-4 text-lg leading-7 font-semibold leading-none tracking-wider">Incongruência</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@include('admin.layout.footer')
@endsection