<div id="filtersCard" class="bg-light dark:bg-primary mb-5 order-1 card sm:order-2 sm:mb-0 sm:ml-5 sm:w-4/12" aria-visible="false">
	<div class="card-header leading-loose bg-white dark:bg-darker py-0 text-primary-dark dark:text-light font-bold">Filtros</div>
	<div class="card-body p-1">
		<form action="{{ route('admin.test.index') }}" method="get" autocomplete="off">


			<div class="flex-auto">

				<div class="flex flex-wrap">
					<div class="w-full lg:w-12/12">
						<div class="relative w-full mb-3">
							<label for="filterConcordance" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Concordância</label>
							<select id="filterConcordance" name="concordance_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 errors->has('concordance_id') ? ' is-invalid' : '' }}" data-endpoint="api/concordance/" data-labelfield="name" data-valuefield="id" data-current="{{ $concordance_id??old('concordance_id') }}">
								<option value="">Selecione uma Concordância</option>
							</select>
						</div>
					</div>
				</div>
				<div class="flex flex-wrap">
					<div class="w-full lg:w-12/12">
						<div class="relative w-full mb-3">
							<label for="filterPatient" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Paciente</label>
							<select id="filterPatient" name="patient_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150{{ $errors->has('role_id') ? ' is-invalid' : '' }}" data-endpoint="api/patient" data-labelfield="name" data-valuefield="id" data-current="{{ $patient_id??old('patient_id') }}">
                                <option value="">Selecione um Paciente</option>
                            </select>
						</div>
					</div>
				</div>
				<div class="flex flex-wrap">
					<div class="w-full lg:w-6/12">
						<div class="relative w-full mb-3">
							<button type="button" href="#" onclick="window.location.href='{{ route('admin.test.index') }}'" class="w-full text-xs text-white bg-gray-500 hover:bg-gray-200 hover:text-black md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded md:mr-2 sm:mr-0 "><i class="fa fa-times"></i> Limpar filtros</a>
						</div>
					</div>
					<input type="hidden" name="action" value="filter">
					<div class="w-full lg:w-6/12">
						<div class="relative w-full mb-3">
							<button type="submit" class="w-full text-xs text-white bg-red-800 hover:bg-red-200 hover:text-black md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded md:mr-2 sm:mr-0"><i class="fa fa-search"></i> Filtrar</button>
						</div>
					</div>
				</div>
		</form>
	</div>
</div>