@extends('admin.layout.app', [
'title'=>'Dashboard',
'classes'=>[
'bg-light',
'dark:bg-image-dark',
]
])
@section('content')

<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">

	<div class="mt-8 bg-white rounded-md dark:bg-darker bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
		<div class="grid grid-cols-1 md:grid-cols-3">
			<div class="p-6">
				<div class="flex items-center">
					<i class="fad fa-user fa-2x text-gray-500 dark:text-primary-dark"></i>
					<div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Pacientes</span></div>
				</div>

				<div class="ml-12">
					<div class="mt-2 text-center leading-none tracking-wider text-gray-500 dark:text-primary-light">
						<div class="number">{{$patient}}</div>
					</div>
				</div>
			</div>

			<div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
				<div class="flex items-center">
					<i class="fad fa-chart-scatter fa-2x text-gray-500 dark:text-primary-dark"></i>
					<div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Testes</span></div>
				</div>

				<div class="ml-12">
					<div class="mt-2 text-center leading-none tracking-wider text-gray-500 dark:text-primary-light">
						<div class="number">{{$test}}</div>
					</div>
				</div>
			</div>
			<div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
				<div class="flex items-center">
					<i class="fad fa-file-word fa-2x text-gray-500 dark:text-primary-dark"></i>
					<div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Palavras</span></div>
				</div>

				<div class="ml-12">
					<div class="mt-2 text-center leading-none tracking-wider text-gray-500 dark:text-primary-light">
						<div class="number">{{$word}}</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@include('admin.layout.footer')
@endsection