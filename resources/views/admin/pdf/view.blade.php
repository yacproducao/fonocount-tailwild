<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teste de {{$test->concordance->name}} do Paciente {{$test->patient->name}}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('/res/images/brand/icone.png') }}" rel="icon" type="image/png">
    <link type="text/css" href="{{mix('/css/layout.css')}}" rel="stylesheet">
    <link type="text/css" href="{{mix('/css/app.css')}}" rel="stylesheet">
    <style>
        html,
        body {
            color: white;
            font-weight: 100;
        }

        html {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }

        body {
            background-color: #12263f;
            padding: 50 70;
            height: 200vh;
        }

        thead tr,
        table thead {
            background-color: #b5b5b5;
        }

        .mt {
            margin-top: 40px;
        }

        table {
            border: 1px solid;
        }

        table th,
        table td {
            border-bottom: 1px solid black;
            padding-left: 1rem;
            padding-right: 1rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            text-align: left;
            color: black;
        }

        tbody tr:nth-child(even) {
            background-color: #fefefe;
        }

        tbody tr:nth-child(odd) {
            background-color: #dfdfdf;
        }

        h1 {
            color: white;
            font-size: 20pt;
            margin-bottom: 30px;
        }

        p {
            font-size: 12pt;
            color: white;
            font-weight: 400;
            text-align: justify;
            margin-bottom: 30px;
        }


        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        h3 {
            margin: 0;
            margin-bottom: 10px;
            color: white;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .flex-center {
            display: flex;
            justify-content: center;
            width: 100%;
        }

        .logo {
            background-position: center;
            width: 100%;
            height: 70px;
            background-size: contain;
            background-repeat: no-repeat;
            background-image: url(<?php echo asset('/res/images/brand/logo-lateral.png') ?>);
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="logo"></div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h1 style="text-align: center;">Teste de {{$test->concordance->name}} do Paciente {{$test->patient->name}}</h1>
        </div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Nome: {{$test->patient->name}}</h3>
        </div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Idade: {{\Carbon\Carbon::parse($test->patient->birth)->diff(\Carbon\Carbon::now())->format('%y Anos')}}</h3>
        </div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Concordancia: {{$test->concordance->name}}</h3>
        </div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Realizado em: {{Carbon\Carbon::parse($test->create_at)->format('d/m/Y')}}</h3>
        </div>
    </div>

    @foreach($types as $type)
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>{{$type->contador}} {{$type->contador==1?'Palavra':'Palavras'}} com <span style="color:{{$type->cor}}">{{$type->name}}</span></h3>
        </div>
    </div>
    @endforeach
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Menor tempo de Resposta: {{$time[0]}}</span></h3>
        </div>
    </div>
    <div class="flex-center position-ref full-height">
        <div class="form-group">
            <h3>Maior tempo de Resposta: {{$time[1]}}</span></h3>
        </div>
    </div>
    <div class="flex-center position-ref full-height mt">
        <table style="width: 100%;">
            <thead>
                <tr>
                    <th>Palavra</th>
                    <th>Tempo</th>
                    <th>Resposta</th>
                </tr>
            </thead>
            <tbody>
                @foreach($test->words as $word)
                <tr>
                    <th>{{$word->name}}</th>
                    <th>{{$word->pivot->time}}</th>
                    <th>{{$word->pivot->response?'Sim':'Não'}}</th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>