@extends('admin.layout.app',[
	'classes'=>[
		'antialiased', 'sm:subpixel-antialiased'
	]
])

@section('content')
	<div class="relative flex items-top justify-center min-w-full min-h-full sm:items-center sm:pt-0">
		<div class="grid grid-cols-1 md:grid-cols-2 min-w-full min-h-full sm:items-center p-12 lg:p-24">
			<div class="p-12 bg-white bg-opacity-75 backdrop-filter backdrop-blur-sm dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg lg:ml-64 flex flex-col items-center">
				<x-u-i.logo width="300" classes="fill-current text-blue-900 dark:text-blue-100" />
				<form action="{{ route('admin.register') }}" method="POST" class="min-w-full p-6 lg:p-12">
					@csrf
					<h1 class="text-2xl font-bold uppercase my-5 text-center">Criar conta</h1>

					<div class="flex flex-col items-stretch mb-4">
						<label for="fld-username" class="text-gray-600">Usuário</label>
						<input type="text" name="username" id="fld-username" placeholder="Usuário" class="rounded-md">
					</div>
					<div class="flex flex-col items-stretch mb-4">
						<label for="fld-password" class="text-gray-600">Senha</label>
						<input type="password" name="password" id="fld-username" placeholder="Senha" class="rounded-md">
					</div>
					<div class="flex flex-col items-center mt-4 pt-16">
						<button class="bg-blue-800 hover:bg-blue-600 text-white font-bold py-2 px-4 uppercase rounded" type="submit">Entrar</button>
					</div>

				</form>
				<div class="px-6 py-6 font-light w-full flex flex-row items-stretch justify-between">
					<a class="underline dark:text-gray-200" href="{{ route('client.login') }}">Já possuo conta</a>
					<a class="underline dark:text-gray-200" href="{{ route('client.home') }}">Voltar para a área do cliente</a>
				</div>
			</div>
			<div></div>
		</div>
	</div>
@endsection

@push('js')
	<script src="{{ mix('js/lavaLamp.js') }}"></script>
@endpush
