@extends('admin.layout.app',[
	'classes'=>[
		'bg-light',
		'dark:bg-dark',
	]
])

@section('content')
	<div class="grid grid-cols-1 lg:grid-cols-2 min-h-full lg:items-center p-12 lg:p-24 min-w-full">
		<div></div>
		<div class="p-12 bg-white bg-opacity-75 backdrop-filter backdrop-blur-sm dark:bg-gray-800 overflow-hidden shadow flex flex-col items-center sm:rounded-lg 2xl:mr-64">
			<x-u-i.symbol width="150" classes="fill-current" />
			<form action="{{ route('admin.login') }}" method="POST" class="min-w-full p-6 lg:p-12">
				@csrf
				<h1 class="text-2xl font-bold uppercase my-5 text-center">Entrar</h1>

				<div class="flex flex-col items-stretch mb-4">
					<label for="fld-email" class="text-gray-600 dark:text-gray-200">Usuário</label>
					<input type="text" name="email" id="fld-email" placeholder="Usuário" class="rounded-md" value="{{ old('email') }}">
					@if($errors->has('email') || $errors->has('password'))
						<span class="text-red-500">{{$errors->first('email')}}</span>
					@endif
				</div>
				<div class="flex flex-col items-stretch mb-4">
					<label for="fld-password" class="text-gray-600 dark:text-gray-200">Senha</label>
					<input type="password" name="password" id="fld-email" placeholder="Senha" class="rounded-md">
				</div>
				<div class="flex flex-row items-stretch mb-4">
					<x-forms.toggle name="remember" caption="Mantenha-me conectado" classes="text-sm text-black dark:text-white" state="true" />
				</div>
				<div class="flex flex-col items-center mt-4 pt-16">
					<button class="bg-blue-800 hover:bg-blue-600 text-white font-bold py-2 px-4 uppercase rounded" type="submit">Entrar</button>
				</div>

			</form>
			<!--<div class="px-6 py-6 font-light w-full flex flex-row items-stretch justify-between">
				<a class="underline dark:text-gray-200 ml-auto" href="{{ route('admin.home') }}">Voltar para a área do cliente</a>
			</div>-->
		</div>
	</div>
@endsection
