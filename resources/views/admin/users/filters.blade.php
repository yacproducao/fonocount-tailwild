<div id="filtersCard" class="bg-light dark:bg-primary mb-5 order-1 card sm:order-2 sm:mb-0 sm:ml-5 sm:w-4/12" aria-visible="false">
	<div class="card-header leading-loose bg-white dark:bg-darker py-0 text-primary-dark dark:text-light font-bold">Filtros</div>
	<div class="card-body p-1">
		<form action="{{ route('admin.user.index') }}" method="get" autocomplete="off">


			<div class="flex-auto">
				<div class="flex flex-wrap">
					<div class="w-full lg:w-12/12">
						<div class="relative w-full mb-3">
							<label for="filterName" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Nome</label>
							<input type="text" name="name" id="filterName" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $name??old('name') }}" placeholder="Nome do Usuario">
						</div>
					</div>
				</div>
				<div class="flex flex-wrap">
					<div class="w-full lg:w-12/12">
						<div class="relative w-full mb-3">
							<label for="filterCpf" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Perfil</label>
							<select id="filterRole" name="role_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150{{ $errors->has('role_id') ? ' is-invalid' : '' }}" data-endpoint="api/role" data-labelfield="name" data-valuefield="id" data-current="{{ $role_id??old('role_id') }}">
                                <option value="">Selecione um Perfil</option>
                            </select>
						</div>
					</div>
				</div>
				<div class="flex flex-wrap">
					<div class="w-full lg:w-12/12">
						<div class="relative w-full mb-3">
							<label for="filterEmail" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Email</label>
							<input type="email" name="email" id="filterEmail" class=" border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $email??old('email') }}" placeholder="Email">
						</div>
					</div>
				</div>
				<div class="flex flex-wrap">
					<div class="w-full lg:w-6/12">
						<div class="relative w-full mb-3">
							<button type="button" href="#" onclick="window.location.href='{{ route('admin.user.index') }}'" class="w-full text-xs text-white bg-gray-500 hover:bg-gray-200 hover:text-black md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded md:mr-2 sm:mr-0 "><i class="fa fa-times"></i> Limpar filtros</a>
						</div>
					</div>
					<input type="hidden" name="action" value="filter">
					<div class="w-full lg:w-6/12">
						<div class="relative w-full mb-3">
							<button type="submit" class="w-full text-xs text-white bg-primary-100 dark:bg-primary-darker  hover:text-primary-dark hover:bg-primary-100 dark:hover:bg-primary-dark dark:hover:text-primary-100 md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded md:mr-2 sm:mr-0"><i class="fa fa-search"></i> Filtrar</button>
						</div>
					</div>
				</div>
		</form>
	</div>
</div>