<div class="px-4 py-4 lg:py-6">
	<div class="flex justify-center mt-4 sm:items-center sm:justify-between">
		<div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
			<div class="flex copyright text-center text-xl-left text-muted">
				&copy; {{ now()->year }} Desenvolvido por
				<a href="https://www.instagram.com/andredevweb/" id='linkcopy' class="font-weight-bold ml-1" target="_blank" title="André Albuquerque">
					<img src="/res/images/logoandrealbuquerque.png" id="logocopy">
				</a>
			</div>
		</div>
	</div>
</div>