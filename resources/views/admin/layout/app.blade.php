<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="min-h-screen">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>{{ (isset($title)?$title." - ":"").env('APP_NAME')}}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="author" content="Fonocount">
	<meta name="theme-color" content="#37cecd">

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Weather PWA">
	<link rel="apple-touch-icon" href="/images/icons/icon-152x152.png">
	<link href="{{ asset('/res/images/brand/icone.png') }}" rel="icon" type="image/png">
	<link type="text/css" href="{{mix('/css/layout.css')}}" rel="stylesheet">
	<link type="text/css" href="{{mix('/css/app.css')}}" rel="stylesheet">
	<script src="{{mix('/js/alpinecomponent.js')}}"></script>
	<script src="{{mix('/js/alpine.js')}}"></script>
	<link rel="manifest" href="/manifest.json">
	@stack('head')
</head>

<body class="{{ isset($classes) && is_array($classes) ? join(' ', array_unique( array_merge(['relative', 'min-h-screen', 'antialiased', 'sm:subpixel-antialiased'], $classes)) ) : join(' ', ['relative', 'min-h-screen', 'antialiased', 'sm:subpixel-antialiased'])}}">
	<div x-data="comp()" x-init="$refs.loading.classList.add('hidden'); setColors(color);" :class="{ 'dark': isDark}">
		@auth
		<div class="flex h-screen antialiased text-gray-900  {{ isset($classes) && is_array($classes) ? join(' ', array_unique($classes)):''}}  dark:text-light ">
			<!-- Loading screen -->
			<div x-ref="loading" class="z-9999999 fixed inset-0  flex items-center justify-center text-2xl font-semibold text-white bg-primary-darker dark:bg-dark">
				Carregando.....
			</div>

			@include('admin.layout.navigation')
			<main>
				<div class="mt-2">
					@yield('content')
				</div>
			</main>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</div>
		@endauth
		@guest
		<div class="relative flex items-top justify-center min-h-screen py-0">
			@yield('content')
		</div>
		@endguest
	</div>
	<script src="{{ mix('js/manifest.js') }}"></script>
	<script src="{{ mix('js/vendor.js') }}"></script>
	<script src="{{ mix('js/app.js') }}"></script>
	<script src="{{ mix('js/script.js') }}"></script>
	<script>
		const comp = () => {
			const getTheme = () => {
				if (window.localStorage.getItem('dark')) {
					return JSON.parse(window.localStorage.getItem('dark'))
				}

				return !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
			}

			const setTheme = (value) => {
				window.localStorage.setItem('dark', value)
			}

			const getColor = () => {
				if (window.localStorage.getItem('color')) {
					return window.localStorage.getItem('color')
				}
				return 'teal'
			}

			const setColors = (color) => {
				const root = document.documentElement
				root.style.setProperty('--color-primary', `var(--color-${color})`)
				root.style.setProperty('--color-primary-50', `var(--color-${color}-50)`)
				root.style.setProperty('--color-primary-100', `var(--color-${color}-100)`)
				root.style.setProperty('--color-primary-light', `var(--color-${color}-light)`)
				root.style.setProperty('--color-primary-lighter', `var(--color-${color}-lighter)`)
				root.style.setProperty('--color-primary-dark', `var(--color-${color}-dark)`)
				root.style.setProperty('--color-primary-darker', `var(--color-${color}-darker)`)
				this.selectedColor = color
				window.localStorage.setItem('color', color)
				//
			}

			const updateBarChart = (on) => {
				const data = {
					data: randomData(),
					backgroundColor: 'rgb(207, 250, 254)',
				}
				if (on) {
					barChart.data.datasets.push(data)
					barChart.update()
				} else {
					barChart.data.datasets.splice(1)
					barChart.update()
				}
			}

			const updateDoughnutChart = (on) => {
				const data = random()
				const color = 'rgb(207, 250, 254)'
				if (on) {
					doughnutChart.data.labels.unshift('Seb')
					doughnutChart.data.datasets[0].data.unshift(data)
					doughnutChart.data.datasets[0].backgroundColor.unshift(color)
					doughnutChart.update()
				} else {
					doughnutChart.data.labels.splice(0, 1)
					doughnutChart.data.datasets[0].data.splice(0, 1)
					doughnutChart.data.datasets[0].backgroundColor.splice(0, 1)
					doughnutChart.update()
				}
			}

			const updateLineChart = () => {
				lineChart.data.datasets[0].data.reverse()
				lineChart.update()
			}

			return {
				loading: true,
				isDark: getTheme(),
				toggleTheme() {
					this.isDark = !this.isDark
					setTheme(this.isDark)
				},
				setLightTheme() {
					this.isDark = false
					setTheme(this.isDark)
				},
				setDarkTheme() {
					this.isDark = true
					setTheme(this.isDark)
				},
				color: getColor(),
				selectedColor: 'red',
				setColors,
				toggleSidbarMenu() {
					this.isSidebarOpen = !this.isSidebarOpen
				},
				isSettingsPanelOpen: false,
				openSettingsPanel() {
					this.isSettingsPanelOpen = true
					this.$nextTick(() => {
						this.$refs.settingsPanel.focus()
					})
				},
				isNotificationsPanelOpen: false,
				openNotificationsPanel() {
					this.isNotificationsPanelOpen = true
					this.$nextTick(() => {
						this.$refs.notificationsPanel.focus()
					})
				},
				isSearchPanelOpen: false,
				openSearchPanel() {
					this.isSearchPanelOpen = true
					this.$nextTick(() => {
						this.$refs.searchInput.focus()
					})
				},
				isMobileSubMenuOpen: false,
				openMobileSubMenu() {
					this.isMobileSubMenuOpen = true
					this.$nextTick(() => {
						this.$refs.mobileSubMenu.focus()
					})
				},
				isMobileMainMenuOpen: false,
				openMobileMainMenu() {
					this.isMobileMainMenuOpen = true
					this.$nextTick(() => {
						this.$refs.mobileMainMenu.focus()
					})
				},
				updateBarChart,
				updateDoughnutChart,
				updateLineChart,
			}
		}
	</script>
	@stack('js')
</body>

</html>