@extends('admin.layout.app', [
'title'=>'Editar Perfil',
'classes'=>[
'bg-light',
'dark:bg-image-dark',
]
])
@section('content')
<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">
    <div class="w-full lg:w-12/12 px-4 mx-auto mt-6">
        <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-light dark:bg-primary border-0">
            <div class="rounded-t bg-white rounded-md dark:bg-darker mb-0 px-6 py-6">
                <div class="text-center flex justify-between">
                    <h6 class="text-primary-dark dark:text-light text-xl font-bold">
                        Perfil
                    </h6>
                </div>
            </div>
            <div class="flex-auto px-4 lg:px-10 py-10 pt-0">

                @if (session('status'))

                <div class="alert flex flex-row items-center bg-green-200 p-5 rounded border-b-2 border-green-300">
                    <div class="alert-icon flex items-center bg-green-100 border-2 border-green-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
                        <span class="text-green-500">
                            <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
                            </svg>
                        </span>
                    </div>
                    <div class="alert-content ml-4">
                        <div class="alert-title font-semibold text-lg text-green-800">
                            Sucesso
                        </div>
                        <div class="alert-description text-sm text-green-600">
                            {{session('status')}}
                        </div>
                    </div>
                </div>
                @endif
                <form action="{{ route('admin.profile.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <h6 class="text-primary-dark dark:text-light text-sm mt-3 mb-6 font-bold uppercase">
                        Informações do Usuario
                    </h6>

                    <div class="flex flex-wrap">
                        <div class="w-full lg:w-12/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Nome*</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend border-gray">
                                        <span class="input-group-text bg-transparent">
                                            <i class="fad fa-user"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="name" id="fld-name" value="{{old('name', auth()->user()->name)}}" placeholder="Nome" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
                                </div>
                            </div>
                            @error('name')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Usuario*</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend border-gray">
                                        <span class="input-group-text bg-transparent">
                                            <i class="fad fa-user"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="user" id="fld-user" placeholder="Usuario" value="{{old('user', auth()->user()->user)}}" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
                                </div>
                            </div>
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300" id="existe" style="display:none">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800" id="textuser">

                                    </div>
                                </div>
                            </div>
                            @error('user')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Email*</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend border-gray">
                                        <span class="input-group-text bg-transparent">
                                            <i class="fad fa-envelope"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="email" id="fld-email" value="{{old('email', auth()->user()->email)}}" placeholder="Email" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
                                </div>
                            </div>

                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300" id="existeemail" style="display:none">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800" id="textemail">

                                    </div>
                                </div>
                            </div>
                            @error('email')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                        <div class="w-full lg:w-12/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Foto de Perfil
                                </label>
                                <label for="file-upload" class="relative cursor-pointer">
                                    <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                        <div class="space-y-1 text-center">

                                            <div style="background-image: url('{{ auth()->user()->avatarImage}}'); background-repeat: no-repeat;background-size: contain;width: 100%;height: 50px;background-position: center;"></div>
                                            <div class="flex text-sm justify-center text-gray-600">
                                                <label for="file-upload" class="relative cursor-pointer rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <input type="file" id="file-upload" name="avatar" class="hidden" accept=".jpeg, .png, .jpg, .gif"><label for="file-upload">Selecionar Arquivo</label>
                                                </label>
                                            </div>
                                            <div id="avatar-upload-name" class="text-xs bg-white text-light dark:text-primary-dark p-2 rounded hidden">
                                            </div>
                                            <p class="text-xs text-primary-dark dark:text-light">
                                                PNG, JPG, GIF com no maximo 4MB
                                            </p>
                                        </div>
                                    </div>
                                </label>
                            </div>

                            @error('avatar')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="flex flex-wrap">
                        <div class="w-full lg:w-12/12 px-4">
                            <div class="md:col-span-5 text-right">
                                <div class="inline-flex w-full items-end">
                                    <button type="submit" class="flex text-center justify-center w-full px-4 py-2 font-medium text-center text-white transition-colors duration-200 rounded-md bg-gray-500 focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="w-full lg:w-12/12 px-4 mx-auto mt-6">
        <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-light dark:bg-primary border-0">
            <div class="rounded-t bg-white rounded-md dark:bg-darker mb-6 mb-0 px-6 py-6">
                <div class="text-center flex justify-between">
                    <h6 class="text-primary-dark dark:text-light text-xl font-bold">
                        Alterar Senha
                    </h6>
                </div>
            </div>
            <div class="flex-auto px-4 lg:px-10 py-10 pt-0">
                <form action="{{ route('admin.profile.password') }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="flex flex-wrap">
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label for="fld-password" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2" htmlfor="grid-password">
                                    Senha
                                </label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fad fa-key text-primary-dark dark:text-black"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" id="fld-password" placeholder="Senha" class="rounded-md password text-primary-dark dark:text-black" required>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text eyes" style="cursor: pointer;" id="eyes">
                                            <i class="fad fa-eye text-primary-dark dark:text-black"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @error('password')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label for="fld-confirm-password" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2" htmlfor="grid-confirm-password">
                                    Confirme a Senha
                                </label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fad fa-key text-primary-dark dark:text-black"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password_confirmation" id="fld-confirm-password" placeholder="Senha" class="rounded-md password text-primary-dark dark:text-black" required>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text eyes" style="cursor: pointer;">
                                            <i class="fad fa-eye text-primary-dark dark:text-black"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @error('password_confirmation')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-12/12 px-4">
                            <div class="inline-flex w-full items-end">
                                <button type="submit" class="w-full px-4 py-2 font-medium text-center text-white transition-colors duration-200 rounded-md bg-gray-500 focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker">Enviar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('admin.layout.footer')
@endsection