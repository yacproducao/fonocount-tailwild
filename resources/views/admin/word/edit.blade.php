@extends('admin.layout.app', [
'title'=>'Adicionar Palavra',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])
@section('content')

<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">
    <div class="w-full lg:w-12/12 px-4 mx-auto mt-6">
        <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-light dark:bg-primary border-0">
            <div class="rounded-t bg-white rounded-md dark:bg-darker mb-0 px-6 py-6">
                <div class="text-center flex justify-between">
                    <h6 class="text-primary-dark dark:text-light text-xl font-bold">
                        Palavra
                    </h6>
                </div>
            </div>
            <div class="flex-auto px-4 lg:px-10 py-10 pt-5">

                @if (session('status'))

                <div class="alert flex flex-row items-center bg-green-200 p-5 rounded border-b-2 border-green-300">
                    <div class="alert-icon flex items-center bg-green-100 border-2 border-green-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
                        <span class="text-green-500">
                            <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
                            </svg>
                        </span>
                    </div>
                    <div class="alert-content ml-4">
                        <div class="alert-title font-semibold text-lg text-green-800">
                            Sucesso
                        </div>
                        <div class="alert-description text-sm text-green-600">
                            {{session('status')}}
                        </div>
                    </div>
                </div>
                @endif
                <form action="{{ route('admin.word.update', ['word'=>$word]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="flex flex-wrap ">
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Palavra
                                </label>
                                <input type="text" placeholder="Palavra" name="name" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{old('name', $word->name)}}" required>
                            </div>
                            @error('name')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-6/12 px-4">
                            <div class="relative w-full mb-3">
                                <label for="filterRole" class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2" htmlfor="grid-role">
                                    Tipo de Palavra
                                </label>
                                <select id="filteType" name="type_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" data-endpoint="api/type" data-labelfield="name" data-valuefield="id" data-current="{{ old('type_id', $word->type_id) }}" data-targetid="" data-updateevent="" required>
                                    <option value="">Selecione um Tipo</option>
                                </select>
                            </div>
                            @error('type_id')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="w-full lg:w-12/12 px-4">
                        <div class="inline-flex w-full items-end">
                            <button type="submit" class="w-full px-4 py-2 font-medium text-center text-white transition-colors duration-200 rounded-md bg-gray-500 focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('admin.layout.footer')
@endsection