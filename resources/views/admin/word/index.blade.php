@extends('admin.layout.app', [
'title'=>'Palavras',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])

@section('content')
<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">
	@if (session('status'))
	<div class="alert flex flex-row items-center bg-green-200 p-5 rounded border-b-2 border-green-300 mb-5">
		<div class="alert-icon flex items-center bg-green-100 border-2 border-green-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
			<span class="text-green-500">
				<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
					<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
				</svg>
			</span>
		</div>
		<div class="alert-content ml-4">
			<div class="alert-title font-semibold text-lg text-green-800">
				Sucesso
			</div>
			<div class="alert-description text-sm text-green-600">
				{{session('status')}}
			</div>
		</div>
	</div>
	@endif
	<div class="flex flex-row items-center justify-items-center">

		<div class="w-full flex flex-wrap">
			<div class="w-full lg:w-8/12 px-4">
				<div class="relative w-full mb-3">
					<h1 class="flex-grow section-title text-gray-500 dark:text-primary-light">Palavras</h1>
				</div>
			</div>
			<div class="w-6/12 lg:w-2/12 px-4">
				<div class="relative w-full mb-3">
					<button class="flex text-center justify-center text-xs w-full text-white hover:text-primary bg-primary hover:bg-primary-100 md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded" @click="window.location.href='{{ route('admin.word.create')}}'">Adicionar</button>
				</div>
			</div>
			<div class="w-6/12 lg:w-2/12 px-4">
				<div class="relative w-full mb-3">
					@if(view()->exists('admin.word.filters'))
					<button class="flex text-center justify-center text-xs w-full text-white bg-gray-500 hover:bg-gray-200 hover:text-black text-black md:mr-2 sm:mr-0 font-bold py-2 px-4 uppercase rounded" data-behaviour="toggle" type="button" data-target="filtersCard">filtros</button>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="flex flex-col sm:flex-row">
		<div class="card flex-grow order-2 sm:order-1">
			<div class="block w-full overflow-x-auto ">
				<table class="items-center w-full bg-transparent border-collapse">
					<thead class="dark:bg-darker">
						<th class="dark:text-primary-dark">{{ __("Nome") }}</th>
						<th class="dark:text-primary-dark">{{__("Tipo")}}</th>
					</thead>
					<tbody>
						@foreach ($words as $word)
						<tr>
							<td>
								<a href="{{ route('admin.word.edit', ['word'=>$word]) }}" class="text-red-500 hover:text-red-600">{{ $word->name }}</a>
							</td>
							<td>
								{{$word->type->name}}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@if($words->hasPages())
				<div class="card-footer text-right">{{ $words->appends(request()->except('page'))->onEachSide(5)->links() }}</div>
				@endif
			</div>
		</div>
		@includeIf('admin.word.filters')
	</div>
</div>
</div>

@include('admin.layout.footer')
@endsection