<aside class="flex-shrink-0 hidden w-64 bg-primary border-r dark:border-primary-darker dark:bg-darker md:block">
	<div class="flex flex-col h-full">
		<!-- Sidebar links -->
		<nav aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">
			<!-- Dashboards links -->

			<a href="{{route('patient.test.index')}}" class="flex items-center p-2  transition-colors rounded-md dark:text-light hover:bg-primary-100 hover:text-primary dark:hover:bg-primary {{request()->is('teste', 'teste/*') ? 'text-primary bg-white dark:bg-primary':'text-white hover:text-primary '}}" role="button" aria-haspopup="true" aria-expanded="{{request()->is('teste', 'teste/*') ? 'true' : 'false'}}">
				<span aria-hidden="true">
					<i class="fad fa-chart-scatter"></i>
				</span>
				<span class="ml-2 text-sm"> Meus Testes </span>
			</a>

		</nav>

		<!-- Sidebar footer -->
		<div class="flex-shrink-0 px-2 py-4 space-y-2">
			<button onclick="event.preventDefault(); document.getElementById('logout-form').submit();" type="button" class="flex items-center justify-center w-full px-4 py-2 text-sm text-primary rounded-md bg-white dark:bg-primary hover:bg-primary-dark focus:outline-none focus:ring focus:ring-primary-dark focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark">
				<span aria-hidden="true">
					<i class="fad fa-running mr-2"></i>
				</span>
				<span>Sair</span>
			</button>
		</div>
	</div>
</aside>
<div class="flex-1 h-full overflow-x-hidden overflow-y-auto">
	<!-- Navbar -->
	<header class="relative bg-primary dark:bg-darker">
		<div class="flex items-center justify-between p-2 border-b dark:border-primary-darker">
			<!-- Mobile menu button -->
			<button @click="isMobileMainMenuOpen = !isMobileMainMenuOpen" class="p-1 transition-colors duration-200 rounded-md text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
				<span class="sr-only">Abrir Menu</span>
				<span aria-hidden="true">
					<svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
					</svg>
				</span>
			</button>

			<!-- Brand -->
			<a href="{{route('admin.home')}}" class="inline-block text-2xl font-bold tracking-wider uppercase text-primary-dark dark:text-light">
				<x-u-i.logo width="150" classes="fill-current" />
			</a>

			<!-- Mobile sub menu button -->
			<button @click="isMobileSubMenuOpen = !isMobileSubMenuOpen" class="p-1 transition-colors duration-200 rounded-md text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
				<span class="sr-only">Abrir Submenu</span>
				<span aria-hidden="true">
					<svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
						<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z" />
					</svg>
				</span>
			</button>
			<x-u-i.drop-down />
			<!-- Mobile main manu -->

			<div x-show="isMobileMainMenuOpen" x-ref="userMenu" x-transition:enter="transition-all transform ease-out" x-transition:enter-start="translate-y-1/2 opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition-all transform ease-in" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="translate-y-1/2 opacity-0" @click.away="isMobileMainMenuOpen = false" @keydown.escape="isMobileMainMenuOpen = false" class="z-999999 absolute right-0 w-48 py-1 bg-primary-light rounded-md shadow-lg top-12 left-1 ring-black ring-opacity-5 dark:bg-dark focus:outline-none" tabindex="-1" role="menu" aria-orientation="vertical" aria-label="Sub Menu">
				<a href="{{route('patient.test.index')}}" class="flex items-center p-2  transition-colors rounded-md dark:text-light hover:bg-primary-100 hover:text-primary dark:hover:bg-primary {{request()->is('teste', 'teste/*') ? 'text-primary bg-white dark:bg-primary':'text-white hover:text-primary '}}" role="button" aria-haspopup="true" aria-expanded="{{request()->is('teste', 'teste/*') ? 'true' : 'false'}}">
					<span aria-hidden="true">
						<i class="fad fa-chart-scatter"></i>
					</span>
					<span class="ml-2 text-sm"> Meus Testes </span>
				</a>
			</div>
	</header>