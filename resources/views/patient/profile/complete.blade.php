@extends('patient.layout.appcontinue',[
'title'=>'Complete o seu Perfil',
'classes'=>[
'bg-light',
'dark:bg-image-dark',
]
])

@section('content')

<form action="{{ route('patient.profile.completeUser') }}" method="POST" enctype="multipart/form-data" class="w-full">
    @csrf
    @method('put')
    <div class="flex justify-center items-start h-screen">
        <div class="w-full lg:w-12/12 px-4 mx-auto pt-6 w-80">
            <div class="relative flex flex-col min-w-0 break-words w-full shadow-lg rounded-lg bg-light dark:bg-primary border-1">
                <div class="rounded-t bg-white rounded-md dark:bg-darker mb-0 px-6 py-6">
                    <div class="text-center flex justify-between">
                        <h6 class="text-primary-dark dark:text-light text-xl font-bold">
                            Complete seu Cadastro
                        </h6>
                    </div>
                </div>
                <div class="flex-auto px-4 lg:px-10 py-10 ">
                    <div class="flex flex-wrap">
                        <div class="w-full lg:w-4/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Data de Nascimento*</label>
                                <input class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" id='birth' placeholder="{{ __('Data de Nascimento*') }}" type="date" name="birth" value="{{ old('birth') }}" required>
                            </div>
                            @error('birth')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                        <div class="w-full lg:w-4/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Telefone/Celular*</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend border-gray">
                                        <span class="input-group-text bg-transparent">
                                            <i class="fad fa-phone"></i>
                                        </span>
                                    </div>
                                    <input class="tel border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="{{ __('(99) 99999-9999') }}" type="text" name="phones" value="{{ old('phones') }}" id='phones'>
                                </div>
                            </div>
                            @error('phones')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-4/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">Sexo*</label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend border-gray">
                                        <span class="input-group-text bg-transparent">
                                            <i class="fad fa-transgender"></i>
                                        </span>
                                    </div>
                                    <select id='sex' name="sex" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" required>
                                        <option value="">Sexo</option>
                                        <option @if(old('sex')=='m' )){{__("selected=='selected'")}}@endif value="m">Masculino</option>
                                        <option @if(old('sex')=='f' )){{__("selected=='selected'")}}@endif value="f">Feminino</option>
                                        <option @if(old('sex')=='o' )){{__("selected=='selected'")}}@endif value="o">Outro</option>
                                    </select>
                                </div>
                            </div>
                            @error('sex')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                        <div class="w-full lg:w-2/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    CEP
                                </label>
                                <input type="text" id="cep" name="zip_code" class="cep border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value="{{ old('zip_code') }}" placeholder="99.999-999">
                            </div>

                            @error('zip_code')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-8/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Endereço
                                </label>
                                <input type="text" id="logradouro" name="street" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="Avenida/Rua Alfa" value="{{ old('street') }}">
                            </div>

                            @error('street')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-2/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Numero
                                </label>
                                <input type="text" id="numero" name="number" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="Nº" value="{{ old('number') }}">
                            </div>

                            @error('number')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-3/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Bairro
                                </label>
                                <input type="text" id="bairro" name="district" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="Bairro" value="{{ old('district') }}">
                            </div>

                            @error('district')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-3/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Complemento
                                </label>
                                <input type="text" id="complemento" name="extra" class="border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" placeholder="Complemento" value="{{ old('extra') }}">
                            </div>

                            @error('extra')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-3/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Estado
                                </label>
                                <select id="filterState" name="state_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" data-endpoint="api/states" data-labelfield="name" data-valuefield="id" data-current="{{ old('state_id') }}" data-targetid="filterCity" data-updateevent="updateCities">
                                    <option value="">Selecione um estado</option>
                                </select>
                            </div>

                            @error('state_id')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-full lg:w-3/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Cidade
                                </label>
                                @if(isset($state_id))
                                <select id="filterCity" name="city_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" data-endpoint="{{ $state_id?'api/cities/{$state_id}':'' }}" data-labelfield="name" data-valuefield="id" data-current="{{   old('city_id') }}">
                                    @else
                                    <select id="filterCity" name="city_id" class="dynamic border-0 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" data-labelfield="name" data-valuefield="id" data-current="{{ old('city_id') }}">
                                        @endif
                                        <option value="">Selecione uma cidade</option>
                                    </select>
                                </select>
                            </div>

                            @error('city_id')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>

                        <div class="w-full lg:w-12/12 px-4">
                            <div class="relative w-full mb-3">
                                <label class="block uppercase text-primary-dark dark:text-light text-xs font-bold mb-2">
                                    Foto de Perfil
                                </label>
                                <label for="file-upload" class="relative cursor-pointer">
                                    <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                        <div class="space-y-1 text-center">
                                            <div class="flex text-sm justify-center text-gray-600">
                                                <label for="file-upload" class="relative cursor-pointer 
                                                             ounded-md font-medium text-primary-dark dark:text-light hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    Selecione uma Imagem
                                                    <input id="file-upload" name="avatar" name="file-upload" type="file" class="hidden" accept=".jpeg, .png, .jpg, .gif">
                                                </label>
                                            </div>
                                            <div id="avatar-upload-name" class="text-xs bg-white text-light dark:text-primary-dark p-2 rounded hidden">
                                            </div>
                                            <p class="text-xs text-primary-dark dark:text-light">
                                                PNG, JPG, GIF com no maximo 4MB
                                            </p>
                                        </div>
                                    </div>
                                </label>
                            </div>

                            @error('avatar')
                            <div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
                                <div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
                                    <span class="text-red-500">
                                        <svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
                                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="alert-content ml-4">
                                    <div class="alert-title font-semibold text-lg text-red-800">
                                        {{ $message }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="flex flex-wrap">
                        <div class="w-full lg:w-12/12 px-4">
                            <div class="md:col-span-5 text-right">
                                <div class="flex gap-4 justify-center border-t p-4">
                                    <button type="submit" class="flex text-center justify-center w-full px-4 py-2 font-medium text-center text-white transition-colors duration-200 rounded-md bg-primary dark:bg-primary-darker  focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection