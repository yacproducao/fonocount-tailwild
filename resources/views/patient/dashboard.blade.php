@extends('patient.layout.app')

<link type="text/css" href="{{mix('/css/mainstyle.css')}}" rel="stylesheet">
<div class="container">
    <div class="slider">

        <div class="box1 box">
            <div class="bg"></div>
            <div class="details">
                <h1 class="text-white">Você está pronto?</h1>
                <p class="text-white">
                    Quando o teste comerçar, você irá ouvir algumas palavras.....
                </p>
                <p class="text-white mobile">
                    Deslize com o dedo para ir para a para a próxima página
                </p>
            </div>
            <div class="illustration">
                <div class="inner"><img src="{{asset('/res/images/balao.png')}}"></div>
            </div>
        </div>

        <div class="box2 box">
            <div class="bg"></div>
            <div class="details">
                <h1 class="text-white">Basta Clicar</h1>
                <p class="text-white mobile">
                    Quando ouvir a palavra reproduzida, apenas clique nela na tela do Tablet/Celular!
                </p>
                <p class="text-white desktop">
                    Quando ouvir a palavra reproduzida, apenas clique nela na tela do seu computador!
                </p>
            </div>
            <div class="illustration">
                <div class="inner"><img src="{{asset('/res/images/travel.png')}}"></div>
            </div>
        </div>

        <div class="box3 box">
            <div class="bg"></div>
            <div class="details">
                <h1 class="text-white">Agora vamos lá!</h1>
                <p class="text-white">
                    Agora que já entendemos o como vamos trabalhar juntos, que tal começarmos?
                </p>
                <p class="text-white mobile">
                    Deslize a tela para a direita para começarmos!
                </p>
                <p class="text-white desktop mt-3">
                    <a class="btn w-full px-4 py-2 font-medium text-center text-white hover:text-white transition-colors duration-200 rounded-md bg-primary dark:bg-primary-darker  focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker" href="{{route('patient.test.new')}}">Clique aqui para começarmos!</a>
                </p>
            </div>

            <div class="illustration">
                <div class="inner"> <img src="{{asset('/res/images/island.png')}}"></div>
            </div>
        </div>
    </div>

    <svg xmlns="http://www.w3.org/2000/svg" class="prev" width="56.898" height="91" viewBox="0 0 56.898 91" class="display:none">
        <path d="M45.5,0,91,56.9,48.452,24.068,0,56.9Z" transform="translate(0 91) rotate(-90)" fill="#fff" />
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" class="next" width="56.898" height="91" viewBox="0 0 56.898 91">
        <path d="M45.5,0,91,56.9,48.452,24.068,0,56.9Z" transform="translate(56.898) rotate(90)" fill="#fff" />
    </svg>
    <div class="trail">
        <div class="box1 active"><i class="fad fa-globe-americas"></i></div>
        <div class="box2"><i class="fad fa-calendar"></i></div>
        <div class="box3"><i class="fad fa-home"></i></div>
    </div>
</div>
<script src="{{mix('js/gsap-latest-beta.js')}}"></script>
<script src="{{mix('js/CSSRulePlugin3.js')}}"></script>
<script src="{{mix('js/sliderapp.js')}}"></script>

</div>