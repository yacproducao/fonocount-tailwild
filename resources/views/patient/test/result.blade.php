@extends('patient.layout.app', [
'title'=>'Resultado',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])
@section('content')

<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">

    <div class="mt-8 bg-white rounded-md dark:bg-darker bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-2">
            <div class="p-6">
                <div class="flex items-center">
                    <i class="fad fa-user fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Paciente: {{auth()->user()->name}}</span></div>
                </div>
            </div>

            <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                <div class="flex items-center">
                    <i class="fad fa-handshake fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Concordancia: {{$concordance->name}}</span></div>
                </div>
            </div>
            <div class="p-6">
                <div class="flex items-center">
                    <i class="fad fa-calendar fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Realizado em: {{Carbon\Carbon::parse($create)->format('d/m/Y')}}</span></div>
                </div>
            </div>
            @foreach($types as $type)
            <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                <div class="flex items-center">
                    <i class="fa fa-file-word fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">{{$type->contador}} {{$type->contador==1?'Palavra':'Palavras'}} com <span class="{{$type->cor}}">{{$type->name}}</span></span></div>
                </div>
            </div>
            @endforeach
            <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                <div class="flex items-center">
                    <i class="fad fa-clock fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Menor tempo de Resposta: {{$time[0]}}</span></div>
                </div>
            </div>
            <div class="p-6">
                <div class="flex items-center">
                    <i class="fad fa-stopwatch fa-2x text-gray-500 dark:text-primary-dark"></i>
                    <div class="ml-4 text-lg leading-7 font-semibold"><span class="leading-none tracking-wider text-gray-500 dark:text-primary-light">Maior tempo de Resposta: {{$time[1]}}</span></div>
                </div>
            </div>
            <div class="border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">

                <a href="{{route('patient.test.index')}}" class="btn p-6 w-full h-full bg-primary hover:bg-primary-100 text-white hover:text-primary">
                    <i class="fad fa-undo-alt fa-2x"></i> <span class="ml-4 text-lg leading-7 font-semibold leading-none tracking-wider">Voltar</span>
                </a>
            </div>
        </div>
    </div>
</div>
@include('patient.layout.footer')

@endsection