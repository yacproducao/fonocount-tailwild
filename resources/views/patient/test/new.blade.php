@extends('patient.layout.app',[
'title'=>'Inicie o Teste',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])

@section('content')

<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">

    <div class="mt-8 bg-gray-300 rounded-md dark:bg-image-dark bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-2">
            <div class="mt-5">
                <a href="{{route('patient.test', ['concordance'=>'congruencia'])}}" class="btn p-6 w-full h-full bg-blue-500 hover:bg-blue-200 text-white hover:text-blue-500">
                    <i class="fad fa-handshake fa-2x"></i> <span class="ml-4 text-lg leading-7 font-semibold leading-none tracking-wider">Congruência</span>
                </a>
            </div>

            <div class="border-t mt-5 border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                <a href="{{route('patient.test', ['concordance'=>'incongruencia'])}}" class="btn p-6 w-full h-full bg-red-500 hover:bg-red-200 text-white hover:text-red-500">
                    <i class="fad fa-handshake fa-2x"></i>
                    <span class="ml-4 text-lg leading-7 font-semibold leading-none tracking-wider">Incongruência</span>
                </a>
            </div>
        </div>
    </div>
</div>

@include('patient.layout.footer')
@endsection