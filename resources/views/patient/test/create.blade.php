@extends('patient.layout.app',[
'title'=>'Inicie o Teste',
'classes'=>[
'bg-gray-300',
'dark:bg-image-dark',
]
])

@section('content')

<div class="px-4 py-4 border-b lg:py-6 dark:border-primary-darker">

    <form method="post" action="{{ route('patient.test.store', ['test'=>$test]) }}" id="form-test" autocomplete="off">
        @csrf
        @method('put')
        <div class="mt-8 bg-gray-300 rounded-md dark:bg-image-dark bg-opacity-75 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div class="mt-5">
                    <button type="button" id="word1" class="btn p-6 w-full h-full {{$cor[0]}}" value="{{$words[0]->id}}">{{$words[0]->name}}</button>
                </div>

                <div class="border-t mt-5 border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                    <button type="button" id="word2" class="btn p-6 w-full h-full {{$cor[1]}}" value="{{$words[1]->id}}">{{$words[1]->name}}</button>
                </div>
            </div>
        </div>

        <input type="hidden" value="00:00" id="timer" name="time">
        <input type="hidden" value="" id="word" name="word">
        <input type="hidden" value="" id="wordnot" name="wordnot">
    </form>
</div>

@include('patient.layout.footer')
@push('js')
<script>
    function startTimer(duration, display) {
        var timer = duration,
            hour,
            minutes, seconds;
        console.log(timer);
        setInterval(function() {
            hour = parseInt(timer / 3600, 10);
            minutes = parseInt((timer % 3600) / 60, 10);
            seconds = parseInt(timer % 60, 10);
            hour = hour < 10 ? "0" + hour : hour;
            console.log(minutes);
            minutes = minutes > 60 ? parseInt(minutes / 60, 10) : minutes;
            minutes = minutes == 60 ? 0 : minutes;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.value = hour + ":" + minutes + ":" + seconds;
            ++timer;
        }, 1000);
    }
    window.onload = function() {
        var duration = 0; // Converter para segundos
        display = document.querySelector('#timer'); // selecionando o timer
        startTimer(duration, display); // iniciando o timer

    };

    $(function() {
        $("#word1").on("click", function() {
            $('#word').val($(this).val());
            $('#wordnot').val($("#word2").val());
            $('#form-test').submit();
        });
        $("#word2").on("click", function() {
            $('#word').val($(this).val());
            $('#wordnot').val($("#word1").val());
            $('#form-test').submit();
        });
    });
</script>
@endpush
@endsection