@extends('layout.app',[
'title'=>'Login',
'classes'=>[
'bg-light',
'dark:bg-image-dark',
]
])

@section('content')

<div class="font-sans">
    <div class="relative flex flex-col sm:justify-center items-center">
        <div class="relative sm:max-w-sm w-full">
            <div class="bg-green-500 shadow-lg  w-full h-full rounded-3xl absolute  transform -rotate-6"></div>
            <div class="bg-primary shadow-lg  w-full h-full rounded-3xl absolute  transform rotate-6"></div>
            <div class="relative w-full rounded-3xl  px-6 py-4 bg-dark dark:bg-light shadow-md">
                <div class="flex w-full justify-center mb-5 mt-2">
                    <x-u-i.symbol width="200" classes="fill-current" />
                </div>
                <form role="form" method="POST" action="{{ route('login_entrar') }}">
                    @csrf
                    <div class="flex flex-col items-stretch mb-4">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend border-gray">
                                <span class="input-group-text bg-transparent">
                                    <i class="fad fa-envelope"></i>
                                </span>
                            </div>
                            <input type="text" name="email" id="fld-email" placeholder="Usuario ou Email" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
                        </div>
                    </div>
                    <div class="flex flex-col items-stretch mb-4">
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend border-gray">
                                <span class="input-group-text">
                                    <i class="fad fa-key"></i>
                                </span>
                            </div>
                            <input type="password" name="password" id="fld-password" placeholder="Senha" class="password border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
                            <div class="input-group-prepend border-gray">
                                <span class="input-group-text eyes" style="cursor: pointer;" id="eyes">
                                    <i class="fad fa-eye"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-row items-stretch mb-2">
                        <x-forms.toggle name="remember" caption="Mantenha-me conectado" classes="text-sm text-white dark:text-primary text-left" state="true" />
                    </div>
                    <div class="flex flex-col items-center mt-0 pt-3">
                        <button class="flex text-center justify-center bg-primary dark:bg-primary-darker w-full py-2 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
                            Entrar
                        </button>
                    </div>
                    <div class="mt-7">
                        <div class="flex justify-center items-center">
                            <label class="mr-2 text-white dark:text-primary"> Não tem cadastro? </label>
                            <a href="{{route('cadastro')}}" class="flex text-center justify-center bg-red-500 dark:bg-darker py-2 px-2 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
                                Clique aqui
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection