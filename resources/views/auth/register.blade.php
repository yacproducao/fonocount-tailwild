@extends('layout.app',[
'title'=>'Cadastro',
'classes'=>[
'bg-light',
'dark:bg-image-dark',
]
])

@section('content')

<div class="font-sans">
	<div class="relative flex flex-col sm:justify-center items-center">
		<div class="relative sm:max-w-sm w-full">
			<div class="bg-green-500 shadow-lg  w-full h-full rounded-3xl absolute  transform -rotate-6"></div>
			<div class="bg-primary shadow-lg  w-full h-full rounded-3xl absolute  transform rotate-6"></div>
			<div class="relative w-full rounded-3xl  px-6 py-4 bg-dark dark:bg-light shadow-md">
				<div class="flex w-full justify-center mb-5 mt-2">
					<x-u-i.symbol width="200" classes="fill-current" />
				</div>
				<form role="form" method="POST" action="{{ route('cadastrar') }}">
					@csrf
					<div class="flex flex-col items-stretch mb-4">
						<div class="input-group input-group-alternative">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text bg-transparent">
									<i class="fad fa-user"></i>
								</span>
							</div>
							<input type="text" name="name" id="fld-name" placeholder="Nome" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
						</div>

						@error('name')
						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800">
									{{ $message }}
								</div>
							</div>
						</div>
						@endif
					</div>
					<div class="flex flex-col items-stretch mb-4">
						<div class="input-group input-group-alternative">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text bg-transparent">
									<i class="fad fa-user"></i>
								</span>
							</div>
							<input type="text" name="user" id="fld-user" placeholder="Usuario" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
						</div>
						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300" id="existe" style="display:none">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800" id="textuser">

								</div>
							</div>
						</div>
						@error('user')
						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800">
									{{ $message }}
								</div>
							</div>
						</div>
						@endif
					</div>
					<div class="flex flex-col items-stretch mb-4">
						<div class="input-group input-group-alternative">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text bg-transparent">
									<i class="fad fa-envelope"></i>
								</span>
							</div>
							<input type="text" name="email" id="fld-email" placeholder="Email" class="border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
						</div>


						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300" id="existeemail" style="display:none">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800" id="textemail">

								</div>
							</div>
						</div>
						@error('email')
						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800">
									{{ $message }}
								</div>
							</div>
						</div>
						@endif
					</div>
					<div class="flex flex-col items-stretch mb-4">
						<div class="input-group input-group-alternative">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text">
									<i class="fad fa-key"></i>
								</span>
							</div>
							<input type="password" name="password" id="fld-password" placeholder="Senha" class="password border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text eyes" style="cursor: pointer;" id="eyes">
									<i class="fad fa-eye"></i>
								</span>
							</div>
						</div>

						@error('password')
						<div class="alert flex flex-row items-center bg-red-200 p-2 mb-2 rounded border-b-2 border-red-300">
							<div class="alert-icon flex items-center bg-red-100 border-2 border-red-500 justify-center h-8 w-8 flex-shrink-0 rounded-full">
								<span class="text-red-500">
									<svg fill="currentColor" viewBox="0 0 20 20" class="h-6 w-6">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</span>
							</div>
							<div class="alert-content ml-4">
								<div class="alert-title font-semibold text-lg text-red-800">
									{{ $message }}
								</div>
							</div>
						</div>
						@endif
					</div>
					<div class="flex flex-col items-stretch mb-4">
						<div class="input-group input-group-alternative">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text">
									<i class="fad fa-key"></i>
								</span>
							</div>
							<input type="password" name="password_confirmation" id="fld-password" placeholder="Confirme a Senha" class="password border-1 px-3 py-3 placeholder-gray-500 text-primary-dark  bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150 ">
							<div class="input-group-prepend border-gray">
								<span class="input-group-text eyes" style="cursor: pointer;" id="eyes">
									<i class="fad fa-eye"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="flex flex-col items-center mt-0 pt-3">
						<button class="bg-primary dark:bg-primary-darker w-full py-2 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
							Registrar
						</button>
					</div>
					<div class="mt-7">
						<div class="flex justify-center items-center">
							<label class="mr-2 text-white dark:text-primary"> Já tem cadastro? </label>
							<a href="{{route('login')}}" class="bg-red-500 dark:bg-darker py-2 px-2 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
								Clique aqui
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection