//countup
function countUp(numberElement) {
	let current = 0;
	const total = parseInt(numberElement.textContent);
	const increment = Math.ceil(total / 100);

	function step() {
		current += increment;
		if (current > total) current = total;
		numberElement.textContent = current.toLocaleString();
		(current !== total) && requestAnimationFrame(step);
	}
	step();
}

document.querySelectorAll('.number').forEach(elem => countUp(elem));

//mostrar senha
let btn = document.querySelectorAll('.eyes');
let input = document.querySelectorAll('.password');
if (btn)
	btn.forEach(element => {
		element.addEventListener("click", function () {
			element.classList.toggle('visible');
			if (element.classList.contains('visible')) {
				document.querySelectorAll('.eyes>i').forEach(el => {
					el.classList.remove('fa-eye');
				});
				document.querySelectorAll('.eyes>i').forEach(el => {
					el.classList.add('fa-eye-slash');
				});
				input.forEach(el => {
					el.setAttribute('type', 'text');
				});
			} else {
				document.querySelectorAll('.eyes>i').forEach(el => {
					el.classList.remove('fa-eye-slash');
				});
				document.querySelectorAll('.eyes>i').forEach(el => {
					el.classList.add('fa-eye');
				});
				input.forEach(el => {
					el.setAttribute('type', 'password');
				});
			}
		});
	});



$('body').on({
	click: function (e) {
		var select = document.getElementById('filterPatient');
		var patient = select.options[select.selectedIndex].value;
		if (patient != "")
			window.location.href = `${process.env.APP_URL}/teste/novo/criar/congruencia/${patient}`;
	}
}, '#congruenciaId');
$('body').on({
	click: function (e) {
		var select = document.getElementById('filterPatient');
		var patient = select.options[select.selectedIndex].value;
		if (patient != "")
			window.location.href = `${process.env.APP_URL}/teste/incongruencia/${patient}`;
	}
}, '#incongruenciaId');
$('body').on({
	ready: function (e) {
		document.querySelectorAll(".patient").forEach(el => {
			el.style.display = "none";
		});
		document.querySelectorAll(".require").forEach(el => {
			el.removeAttribute('required');
		});
	},
	change: function (evt) {
		if (this.options[this.selectedIndex].text == "Paciente") {
			document.querySelectorAll(".patient").forEach(el => {
				el.style.display = "block";
			});
			document.querySelectorAll(".require").forEach(el => {
				el.setAttribute('required', 'required');
			});
		} else {
			document.querySelectorAll(".patient").forEach(el => {
				el.style.display = "none";
			});
			document.querySelectorAll(".require").forEach(el => {
				el.removeAttribute('required');
			});
		}
	}
}, '#filterRole');
function limpa_formulário_cep() {
	document.getElementById("logradouro").value = "";
	document.getElementById("complemento").value = "";
	document.getElementById("bairro").value = "";
	document.getElementById("filterState").value = "";
	document.getElementById("filterCity").value = "";
}
$('body').on({
	blur: function (evt) {
		let cep = $(this).val().replace(/\D/g, '');
		if (cep != "" && /^[0-9]{8}$/.test(cep))
			$.ajax({
				url: `${process.env.APP_URL}/api/cep/${cep}`,
				dataType: 'json',
				success: function (resposta) {
					console.log(resposta.state_id);
					$("#logradouro").val(resposta.logradouro);
					$("#complemento").val(resposta.complemento);
					$("#bairro").val(resposta.bairro);
					$('#filterState').attr('data-current', resposta.state.id).val(resposta.state.id);
					$('#filterCity').attr('data-current', resposta.city.id).val(resposta.city.id);
					$('#filterState').get(0).dispatchEvent(new Event("change"));
				}
			});
		else {
			limpa_formulário_cep();
			alert("Formato de CEP inválido.");
		}
	},
}, '#cep');
let vinculado = document.querySelectorAll(".vinculado");
if (vinculado)
	vinculado.forEach(el => {
		el.style.display = "none";
	});


//mascaras
import VMasker from 'vanilla-masker';

function inputHandler(masks, max, event) {
	var c = event.target;
	var v = c.value.replace(/\D/g, '');
	var m = c.value.length > max ? 1 : 0;
	VMasker(c).unMask();
	VMasker(c).maskPattern(masks[m]);
	c.value = VMasker.toPattern(v, masks[m]);
}
function masks() {
	var tel = document.querySelectorAll('.tel');
	if (tel)
		tel.forEach(el => {
			var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
			VMasker(el).maskPattern(telMask[0]);
			el.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
		});
	var cpf = document.querySelectorAll('.cpf');
	if (cpf)
		cpf.forEach(el => {
			var cpfMask = ['999.999.999-99'];
			VMasker(el).maskPattern(cpfMask[0]);
			el.addEventListener('input', inputHandler.bind(undefined, cpfMask, 13), false);
		});
	var cnpj = document.querySelectorAll('.cnpj');
	if (cnpj)
		cnpj.forEach(el => {
			var cnpjMask = ['99.999.999/9999-99'];
			VMasker(cnpj).maskPattern(cnpjMask[0]);
			el.addEventListener('input', inputHandler.bind(undefined, cnpjMask, 18), false);
		});
	var cep = document.querySelectorAll('.cep');
	if (cep)
		cep.forEach(el => {
			var cepMask = ['99.999-999'];
			VMasker(cep).maskPattern(cepMask[0]);
			el.addEventListener('input', inputHandler.bind(undefined, cepMask, 10), false);
		});
}

masks();

import axios from 'axios';

const valFromPath = (obj, path) => {

	if (/\+/g.test(path)) {
		let fields = path.split(/\+/g);
		let res = [];

		fields.forEach(field => {
			let levels = field.split(/\./g);

			if (levels.length > 1)
				res.push(valFromPath(obj[levels.shift()], levels.join('.')));
			else
				res.push(obj[levels.join('.')]);
		});

		return res.join(' - ');
	} else {
		let levels = path.split(/\./ig);
		if (levels.length > 1)
			return valFromPath(obj[levels.shift()], levels.join('.'));
		else
			return obj[path];
	}

}

const updateDynamicSelects = (selectGroup) => {
	selectGroup.forEach(sel => {
		let opts = {
			endpoint: sel.getAttribute('data-endpoint'),
			labelField: sel.getAttribute('data-labelfield'),
			valueField: sel.getAttribute('data-valuefield'),
			currentValue: sel.getAttribute('data-current'),
			api_token: sel.closest('form').getAttribute('data-api_token'),
			target: sel.getAttribute('data-targetid'),
			targetclass: sel.getAttribute('data-targetclass'),
			updateEvent: sel.getAttribute('data-updateevent'),
			extra: sel.getAttribute('data-extrafields')
		};

		if (opts.updateEvent) {
			let target = opts.target ? document.getElementById(opts.target) : sel.closest('.item').querySelector('.' + opts.targetclass);
			if (target) {
				let events = opts.updateEvent.split('|');
				events.forEach(function (event) {
					switch (event) {
						case "updateStates":
							sel.addEventListener('change', (event) => {
								let dynSelects = document.getElementById(opts.target);
								if (event.target.value) {
									dynSelects.setAttribute('data-endpoint', `api/states/${event.target.value}`);
								} else {
									dynSelects.removeAttribute('data-endpoint');
								}
								updateDynamicSelects([dynSelects]);
							});
							break;
						case "updateCities":
							sel.addEventListener('change', (event) => {
								console.log(opts.target);
								let dynSelects = document.getElementById(opts.target);
								if (event.target.value) {
									dynSelects.setAttribute('data-endpoint', `api/cities/${event.target.value}`);
								} else {
									dynSelects.removeAttribute('data-endpoint');
								}
								updateDynamicSelects([dynSelects]);
							});
							break;
					}
				});
				//target.setAttribute('data-endpoint',`api/cities/${opts.currentValue}`);
			}

		}

		if (opts.endpoint) {
			axios.get(`/${opts.endpoint}`, {
				headers: {
					'Authorization': `Bearer ${opts.api_token}`
				}
			}).then(response => {
				let data = response.data;
				Array.from(sel.querySelectorAll('option')).filter(opt => !['', '0'].includes(opt.getAttribute('value'))).forEach(opt => opt.remove());
				for (let [indx, entry] of data.entries()) {
					let extraFields = opts.extra ? opts.extra.split(/\|/) : null;
					let opt = document.createElement('option');
					opt.setAttribute('value', valFromPath(entry, opts.valueField));
					if (extraFields)
						extraFields.forEach(ef => {
							opt.setAttribute('data-' + ef.replace(/[^\w]/, '_'), valFromPath(entry, ef));
						});
					if (opts.currentValue == valFromPath(entry, opts.valueField))
						opt.setAttribute('selected', '');
					opt.textContent = valFromPath(entry, opts.labelField);
					sel.appendChild(opt);
				}
				let e = new Event('change', { bubbles: true });
				sel.dispatchEvent(e);
			}).catch(error => {
				console.log(error);
			});
		} else
			Array.from(sel.querySelectorAll('option')).filter(opt => opt.getAttribute('value') != 0).forEach(opt => opt.remove());
	});
}

updateDynamicSelects(Array.from(document.querySelectorAll('select.dynamic')));


$('body').on({
	change: function (evt) {
		document.getElementById('avatar-upload-name').innerHTML = this.value;
		if (this.value != "") {
			document.getElementById('avatar-upload-name').classList.add('block');
			document.getElementById('avatar-upload-name').classList.remove('hidden');
		} else {
			document.getElementById('avatar-upload-name').classList.add('hidden');
			document.getElementById('avatar-upload-name').classList.remove('block');
		}
	}
}, '#file-upload');
// All javascript code in this project for now is just for demo DON'T RELY ON IT

const random = (max = 100) => {
	return Math.round(Math.random() * max) + 20
}

const randomData = () => {
	return [
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
		random(),
	]
}

const months = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Aug', 'Set', 'Out', 'Nov', 'Dez']

const cssColors = (color) => {
	return getComputedStyle(document.documentElement).getPropertyValue(color)
}

const getColor = () => {
	return window.localStorage.getItem('color') ?? 'teal'
}

const colors = {
	primary: cssColors(`--color-${getColor()}`),
	primaryLight: cssColors(`--color-${getColor()}-light`),
	primaryLighter: cssColors(`--color-${getColor()}-lighter`),
	primaryDark: cssColors(`--color-${getColor()}-dark`),
	primaryDarker: cssColors(`--color-${getColor()}-darker`),
}

$('body').on({
	keyup: function (evt) {
		var nomeUsuario = this.value;
		var id = document.getElementById("idUser")?document.getElementById("idUser").value:'';
		if (this.value != "") {
			$.ajax({
				url: `${process.env.APP_URL}/api/verificauser/${nomeUsuario}/${id}`,
				dataType: 'json',
				success: function (resposta) {
					console.log(resposta);
					if (resposta == 1) {
						document.getElementById('existe').style.display = "flex";
						document.getElementById('textuser').innerHTML = "Usuario já existe";
					} else {
						document.getElementById('existe').style.display = "none";
					}
				}
			});
		} else {
			document.getElementById('existe').style.display = "flex";
			document.getElementById('textuser').innerHTML = "Usuario Obrigatório";
		}
	},
	keypress: function (e) {
		if (e.which != 13) {
			if (!checkChar(e)) {
				e.preventDefault();
			}
		}
	}
}, "input[name='user']");

$('body').on({
	keyup: function (evt) {
		var email = this.value;
		var id = document.getElementById("idUser")?document.getElementById("idUser").value:'';
		if (this.value != "") {
			$.ajax({
				url: `${process.env.APP_URL}/api/verificaemail/${email}/${id}`,
				dataType: 'json',
				success: function (resposta) {
					console.log(resposta);
					if (resposta == 1) {
						document.getElementById('existeemail').style.display = "flex";
						document.getElementById('textemail').innerHTML = "Email já existe";
					} else {
						document.getElementById('existeemail').style.display = "none";
					}
				}
			});
		} else {
			document.getElementById('existeemail').style.display = "flex";
			document.getElementById('textemail').innerHTML = "Email Obrigatório";
		}
	}
}, "input[name='email']");

function checkChar(e) {
	var char = String.fromCharCode(e.keyCode);
	var pattern = '[a-zA-Z0-9]';
	if (e.which == 13) {
		return false;
	} else if (char.match(pattern)) {
		return true;
	}
}

if ('serviceWorker' in navigator) {
	window.addEventListener('load', () => {
		navigator.serviceWorker.register('/worker.js')
			.then((reg) => {
				console.log('Service worker registered.', reg);
			});
	});
}