$("body").on({
	click: function(evt){
		evt.preventDefault();
		let target = $('#'+$(this).data("target"));
		if(target.attr('aria-visible') == 'false')
			target.attr('aria-visible', 'true');
		else
			target.attr('aria-visible', 'false');
	}
}, '[data-behaviour="toggle"][data-target]');
