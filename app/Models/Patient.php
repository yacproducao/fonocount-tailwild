<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
	use HasFactory, SoftDeletes, Uuids;

	protected $fillable = [
		'name', 'cpf', 'phones', 'sex', 'birth'
	];

	protected $casts = [
		"phones" => "json"
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'delete_at'
	];

	public function address()
	{
		return $this->morphOne('App\Models\Address', 'addressable');
	}
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function tests()
	{
		return $this->hasMany('App\Models\Test');
	}
}
