<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use HasFactory, Uuids;

    public $timestamps = false;

	protected $fillable = [
		'event', 'ip', 'url'
	];

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}
    public function loggable()
	{
		return $this->morphTo();
	}
}
