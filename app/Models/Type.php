<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use HasFactory, SoftDeletes, Uuids;
    public $timestamps = false;
	protected $fillable = [
		'name'
	];

    public function words()
    {
        return $this->hasMany('App\Models\Word');
    }

    public function log()
    {
        return $this->morphMany('App\Models\Log','loggable');
    }
}
