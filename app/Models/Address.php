<?php

namespace App\Models;

use App\Casts\General\ZipCode;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory, Uuids, SoftDeletes;
    public $timestamps = false;

	protected $fillable = [
		'state_id', 'city_id',  'street', 'extra', 'district', 'zip_code', 'number'
	];

	protected $casts = [
		'zip_code'=> ZipCode::class,
	];

    public function addressable()
    {
    	return $this->morphTo();
    }

    public function country()
    {
    	return $this->belongsTo('App\Models\Country');
    }

    public function state()
    {
    	return $this->belongsTo('App\Models\State');
    }

    public function city()
    {
    	return $this->belongsTo('App\Models\City');
    }
}
