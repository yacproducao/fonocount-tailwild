<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	use Uuids;

	public $timestamps = false;

	protected $fillable = [
		'name', 'capital'
	];

	protected $hidden = [
	];

	protected $casts = [
		'capital' => 'boolean',
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function cities()
	{
		return $this->hasMany(City::class);
	}

	public function timezones()
	{
		return $this->morphToMany(Timezone::class, 'timezoneable');
	}
}
