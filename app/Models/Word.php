<?php

namespace App\Models;

use App\Filters\WordFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Word extends Model
{
    use HasFactory, SoftDeletes, Uuids;
	protected $fillable = [
		'name'
	];

	public function scopeFilter(Builder $builder, $request)
	{
		return (new WordFilter($request))->filter($builder);
	}
    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }
	
    public function log()
    {
        return $this->morphMany('App\Models\Log','loggable');
    }

    public function tests()
    {
        return $this->belongsToMany('App\Models\Test')->using('App\Models\TestWord')->withPivot('response', 'time');
    }
}
