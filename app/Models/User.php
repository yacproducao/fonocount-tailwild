<?php

namespace App\Models;

use App\Filters\UserFilter;
use App\Traits\Uuids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravolt\Avatar\Facade as Avatar;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, Uuids, SoftDeletes;

    protected $fillable = [
        'name',
        'user',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

	protected $dates = [
		'created_at', 'updated_at'
	];

    protected $casts = [
		'active'=>"boolean",
        'email_verified_at' => 'datetime',
    ];


	public function addresses()
	{
		return $this->morphMany(Address::class, 'addressable');
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}
	public function log()
	{
		return $this->morphMany('App\Models\Log','loggable');
	}

	public function avatar()
	{
		return $this->morphOne('App\Models\Avatar', 'imageable')->where('type','avatar');
	}
	public function role()
	{
		return $this->belongsTo('App\Models\Role');
	}
	public function getAvatarImageAttribute()
	{
		if($this->avatar){
			return '/storage/avatar/'.$this->avatar->filename;
		}else{
			return Avatar::create(mb_strtoupper($this->name))->toBase64();
		}
	}

	public function scopeFilter(Builder $builder, $request)
	{
		return (new UserFilter($request))->filter($builder);
	}
	public function getIsAdminAttribute()
    {
        return $this->role->isAdmin;
    }

	public function patient()
	{
		return $this->hasOne(Patient::class);
	}

}
