<?php

namespace App\Models;

use App\Traits\HasParentModel;

class Avatar extends Image
{
    use HasParentModel;
}
