<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TestWord  extends Pivot
{
    public $timestamps = false;

    protected $table = "test_word";

	protected $fillable = [
		'response',
        'time'
	];

    public function word()
    {
        return $this->belongsTo('App\Models\Word');
    }
    public function test()
    {
        return $this->belongsTo('App\Models\Test');
    }
}
