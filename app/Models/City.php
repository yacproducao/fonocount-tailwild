<?php

namespace App\Models;

use App\Traits\Uuids;
use App\Filters\CitiesFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class City extends Model
{
	use Uuids;

	public $timestamps = false;

	protected $fillable = [
		'name', 'is_capital'
	];

	protected $hidden = [
	];

	protected $casts = [
		'capital' => 'boolean',
	];

	public function state()
	{
		return $this->belongsTo(State::class);
	}

	public function timezones()
	{
		return $this->morphToMany(Timezone::class, 'timezoneable');
	}

	/*public function scopeFilter(Builder $builder, $request)
	{
		return (new CitiesFilter($request))->filter($builder);
	}*/
}
