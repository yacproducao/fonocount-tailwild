<?php

namespace App\Models\Documents;

use App\Casts\Documents\Cnpj as CnpjNumber;
use App\Traits\HasParentModel;
use App\Traits\Uuids;

class Cnpj extends Document
{
	use HasParentModel, Uuids;

	protected $casts = [
		'number' => CnpjNumber::class,
	];

}
