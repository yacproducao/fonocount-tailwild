<?php

namespace App\Models\Documents;

use App\Traits\HasUuidChildModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory, HasUuidChildModel;

	protected $fillable = [
		'number'
	];

	protected $hidden = [
		'type'
	];

	public function owner()
	{
		return $this->morphTo();
	}

	public function documentable()
	{
		return $this->morphTo();
	}
}
