<?php

namespace App\Models\Documents;

use App\Casts\Documents\Cpf as CpfNumber;
use App\Traits\HasParentModel;
use App\Traits\Uuids;

class Cpf extends Document
{
    use HasParentModel, Uuids;

	protected $casts = [
		'number' => CpfNumber::class,
	];
}
