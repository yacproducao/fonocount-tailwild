<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Concordance extends Model
{
    use HasFactory, Uuids;
    
    public $timestamps = false;

	protected $fillable = [
		'name'
	];
    public function someFunction()
    {
        return $this->table();

    }
    public function log()
    {
        return $this->morphMany('App\Models\Log','loggable');
    }
	public function tests()
	{
		return $this->hasMany('App\Models\Test');
	}
}
