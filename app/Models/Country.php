<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	use Uuids;

	public $timestamps = false;

    protected $fillable = [
    	'phone_code', 'iso', 'iso3', 'name', 'full_name'
    ];

    protected $casts =[
		'emojis'=>'array',
    	'created_at' => 'datetime',
    	'updated_at' => 'datetime',
    ];

    public function states()
    {
    	return $this->hasMany(State::class);
    }

    public function capital()
    {
    	return $this->hasOneThrough(
			City::class,
			State::class
		)->where('is_capital',true);
    }

	public function timezones()
	{
		return $this->morphToMany(Timezone::class, 'timezoneable');
	}

	public function translations()
	{
		return $this->morphMany(Translation::class, 'translatable');
	}
}
