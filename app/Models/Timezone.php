<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    use HasFactory, Uuids;

	public $timestamps = false;

	public function countries()
	{
		return $this->morphedByMany(Country::class, 'timezoneable');
	}

	public function state()
	{
		return $this->morphedByMany(State::class, 'timezoneable');
	}

	public function city()
	{
		return $this->morphedByMany(City::class, 'timezoneable');
	}
}
