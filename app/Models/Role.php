<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends Model
{
    use HasFactory, Uuids;

	protected $fillable = [
		'name', 'description'
	];

	public function user()
    {
        return $this->hasOne(User::class);
    }
	public function permissions()
	{
		return $this->belongsToMany('App\Models\Permission');
	}

	public function getIsAdminAttribute()
	{
		return $this->attributes['name'] === 'Administrador';
	}
}
