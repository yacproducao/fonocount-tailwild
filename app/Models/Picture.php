<?php

namespace App\Models;

use App\Traits\HasParentModel;

class Picture extends Image
{
    use HasParentModel;
}
