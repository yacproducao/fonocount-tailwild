<?php

namespace App\Models;

use App\Filters\TestFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class Test extends Model
{
    use HasFactory, SoftDeletes, Uuids;


    protected $dates = [
        'created_at',
        'updated_at',
        'delete_at'
    ];


    public function words()
    {
        return $this->belongsToMany('App\Models\Word')->using('App\Models\TestWord')->withPivot('response', 'time');
    }

    public function log()
    {
        return $this->morphMany('App\Models\Log', 'loggable');
    }

    public function getDaAttribute()
    {
        $type = Type::where('name', 'BA')->first()->id;
        $ba = $this->words()->select(DB::raw('count(id) as contador'))->where([['test_word.response', true], ['type_id', $type]])->first();
        return $ba->contador;
    }
    public function getBaAttribute()
    {
        $type = Type::where('name', 'DA')->first()->id;
        $da = $this->words()->select(DB::raw('count(id) as contador'))->where([['test_word.response', true], ['type_id', $type]])->first();
        return $da->contador;
    }
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }
    public function concordance()
    {
        return $this->belongsTo('App\Models\Concordance');
    }

	public function scopeFilter(Builder $builder, $request)
	{
		return (new TestFilter($request))->filter($builder);
	}
}
