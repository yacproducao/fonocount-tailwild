<?php

namespace App\Models;

use App\Casts\Images\Dimension;
use App\Traits\HasUuidChildModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory, HasUuidChildModel;

	protected $fillable = [
		'filename', 'format', 'dimensions'
	];

	protected $casts=[
		'dimensions' => Dimension::class,
	];

	public function log()
	{
		return $this->morphMany('App\Models\Log','loggable');
	}

	public function imageable()
	{
		return $this->morphTo();
	}
}
