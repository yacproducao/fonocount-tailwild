<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permission extends Model
{
    use HasFactory, Uuids;

	protected $fillable = [
		'name', 'key', 'controller', 'method'
	];

	public function roles()
    {
    	return $this->belongsToMany('App\Models\Role');
    }
}
