<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'reason' => ['nullable', 'string'],
            'segmentation_id' => ['required', 'string'],
            'cnpj' => ['nullable', 'string'],
            'email' => ['nullable', 'email'],
            'url' => ['nullable', 'url'],
            'phone' => ['nullable','string'],
            'zip_code' => ['nullable', 'string'],
            'street' => ['nullable', 'string'],
            'number' => ['nullable', 'string'],
            'district' => ['nullable', 'string'],
            'extra' => ['nullable', 'string'],
            'state_id' => ['nullable', 'string'],
            'city_id' => ['nullable', 'string'],
            'note' => ['nullable', 'string'],
            'name_gestor' => ['nullable'],
            'phone_gestor' => ['nullable'],
            'email_gestor' => ['nullable'],
            'profile_id' => ['nullable'],
            'id_remove_gestaos' => ['nullable'],
            'id_gestor' => ['nullable'],
            'brand' => ['nullable', 'mimes:jpg,jpeg, gif,png', 'max:4000'],
            'document' => ['nullable', 'mimes:jpg,jpeg,pdf,doc,docx,png', 'max:4000']
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        $messages = [
            'brand.mimes' => __('Tipo de imagem não aceito, somente jpg, jpeg, gif e png'),
            'document.mimes' => __('Tipo de documento não aceito, somente jpg, jpeg, pdf, doc, docx e png'),
            'brand.max' => __('Tamanho muito grande, tamanho maximo de 4MB'),
            'document.max' => __('Tamanho muito grande, tamanho maximo de 4MB'),
            'url.url' => __('Por favor, coloque uma url valida, verifique se tem https://'),
            'name.required' => __('A Nome é requerida'),
            'segmentation_id.required' => __('O Seguimento é requerida'),
            'email.required' => __('O Email é requerida')
        ];
        return $messages;
    }
}

