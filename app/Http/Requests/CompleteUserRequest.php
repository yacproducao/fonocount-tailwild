<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompleteUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birth' => ['required', 'date'],
            'phones' => ['required', 'min:13'],
            'sex' => ['required'],
            'zip_code' => ['nullable', 'string'],
            'street' => ['nullable', 'string'],
            'number' => ['nullable', 'string'],
            'district' => ['nullable', 'string'],
            'extra' => ['nullable', 'string'],
            'state_id' => ['nullable', 'string'],
            'city_id' => ['nullable', 'string'],
            'avatar' => ['nullable', 'mimes:jpg,gif,png', 'max:4000'],
        ];
    }
    public function messages()
    {
        $messages = [
            'birth.required' => __('Por favor, digite a Data de Nascimento'),
            'phones.required' => __('Por favor, digite o Telefone para contato'),
            'sex.required' => __('Por favor, selecione um sexo'),
            'phones.min' => __('Nome deve ter no minimo 13 caracteres'),
            'avatar.mimes' => __('Tipo de imagem não aceito, somente jpg, jpeg, gif e png'),
            'avatar.max' => __('Tamanho muito grande, tamanho maximo de 4MB'),
        ];
        return $messages;
    }
}
