<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;


class ProfileAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', Rule::unique((new User)->getTable())->ignore(auth()->id())],
            'user' => ['required',  Rule::unique((new User)->getTable())->ignore(auth()->id())],
            'avatar' => ['nullable', 'mimes:jpg,gif,png', 'max:4000'],
        ];
    }
    public function messages()
    {
        $messages = [
            'name.required' => __('Por favor, digite o nome'),
            'name.min' => __('Nome deve ter no minimo 3 caracteres'),
            'email.required' => __('Por favor, digite o Email'),
            'email.unique' => __('Email ja existe'),
            'user.required' => __('Por favor, digite o Usuário'),
            'user.unique' => __('Usuário ja existe'),
            'avatar.mimes' => __('Tipo de imagem não aceito, somente jpg, jpeg, gif e png'),
            'avatar.max' => __('Tamanho muito grande, tamanho maximo de 4MB'),
        ];
        return $messages;
    }
}
