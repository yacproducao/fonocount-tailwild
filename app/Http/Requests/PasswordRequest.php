<?php

namespace App\Http\Requests;

use App\Rules\CurrentPasswordCheckRule;
use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['required', 'min:6', 'confirmed'],
            'password_confirmation' => ['required', 'min:6'],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        $messages = [
            'password.required' => __('A nova senha é requerida'),
            'password.min' => __('A nova senha deve ter no minimo 6 caracteres'),
            'password_confirmation.required' => __('Por favor, confirme a senha'),
            'password_confirmation.min' => __('A confirmação da nova senha deve ter no minimo 6 caracteres'),
            'password.confirmed' => __('A confirmação de nova senha não confere'),
        ];
        return $messages;
    }
}
