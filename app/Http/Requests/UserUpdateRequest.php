<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email'],
            'user' => ['required'],
            'role_id' => ['required'],
            'birth' => ['nullable', 'date'],
            'phones' => ['nullable', 'min:13'],
            'sex' => ['nullable'],
            'zip_code' => ['nullable', 'string'],
            'street' => ['nullable', 'string'],
            'number' => ['nullable', 'string'],
            'district' => ['nullable', 'string'],
            'extra' => ['nullable', 'string'],
            'state_id' => ['nullable', 'string'],
            'city_id' => ['nullable', 'string'],
            'avatar' => ['nullable', 'mimes:jpg,gif,png', 'max:4000']
        ];
    }
    public function messages()
    {
        $messages = [
            'name.required' => __('Por favor, digite o nome'),
            'name.min' => __('Nome deve ter no minimo 3 caracteres'),
            'email.required' => __('Por favor, digite o Email'),
            'user.required' => __('Por favor, digite o Usuário'),
            'rule.required' => __('Por favor, escolha um Perfil'),
            'birth.required' => __('Por favor, digite a Data de Nascimento'),
            'phones.required' => __('Por favor, digite o Telefone para contato'),
            'sex.required' => __('Por favor, selecione um sexo'),
            'phones.min' => __('Nome deve ter no minimo 13 caracteres'),
            'avatar.mimes' => __('Tipo de imagem não aceito, somente jpg, jpeg, gif e png'),
            'avatar.max' => __('Tamanho muito grande, tamanho maximo de 4MB'),
        ];
        return $messages;
    }
}
