<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use App\Exceptions\SubdomainNotFound;

class CheckSubDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getHost() == "paciente." . env('APP_DOMAIN') || $request->getHost() == env('APP_DOMAIN')) {
            return $next($request);
        } else {
            throw new SubdomainNotFound();
        }
    }
}
