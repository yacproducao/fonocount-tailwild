<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Role;
use App\Models\Address;
use App\Models\Avatar;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use App\Models\State;
use App\Models\City;
use App\Models\Patient;
use App\Models\Log;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function store(Request $request)
    {
        $email = User::where('email', $request->email)->first();
        $usuario = User::where('user', $request->user)->first();
        if ($email)
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'email' => 'Email já existe',
                ]);
        else if ($usuario)
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'email' => 'Email já existe',
                ]);
        else if ($request->password != $request->password_confirmation)
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'password' => 'Senha não batem',
                ]);
        else {
            $role = Role::where('name', 'like', 'Paciente')->first();
            $user = new User($request->except(['_token', '_method', 'Registrar', 'password', 'password_confirmation']));
            $user->role()->associate($role);
            $user->password = Hash::make($request->password);
            $user->save();

            $patient = new Patient($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'password', 'password_confirmation']));
            $patient->user()->associate($user);
            $patient->save();

            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Novo Paciente com id de Usuario " . $user->id;
            $log->user_id = $user->id;
            $user->log()->save($log);

            Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], true);
            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Usuario fez Login " . $user->id;
            $log->user_id = $user->id;
            $user->log()->save($log);

            return redirect()->route('patient.home');
        }
    }
    protected function create()
    {
        return view('auth.register');
    }
}
