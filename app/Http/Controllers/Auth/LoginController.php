<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Log;
use App\Providers\RouteServiceProvider;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
	/*
	|- -------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	public function login(Request $request)
	{
		$user = User::where('email', $request->email)->orWhere('user', $request->email)->first();

		if ($user) {
			$request->email = $user->email;
			if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->has('remember'))) {
				$log = new Log();
				$log->ip = $request->ip();
				$log->url = $request->url();
				$log->event = "Usuario fez Login " . $user->id;
				$log->user_id = $user->id;
				$user->log()->save($log);
				if ($user->isAdmin)
					return redirect()->route('admin.home');
				else
					return redirect()->route('patient.home');
			} else {
				return back()
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => 'As credenciais informadas não são válidas',
					]);
			}
		} else {
			return back()
				->withInput($request->only('email', 'remember'))
				->withErrors([
					'email' => 'As credenciais informadas não são válidas',
				]);
		}
	}

	public function logout(Request $request)
	{
		$user = User::find(auth()->user()->id);
		$log = new Log();
		$log->ip = $request->ip();
		$log->url = $request->url();
		$log->event = "Usuario fez Logout " . $user->id;
		$log->user_id = $user->id;
		$user->log()->save($log);
		Auth::logout();
		return redirect()->route('login');
	}


	public function showLoginForm()
	{
		return view('auth.login');
	}
}
