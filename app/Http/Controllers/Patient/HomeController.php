<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\Concordance;

class HomeController extends Controller
{

    public function index()
    {
        if (auth()->user()->active)
            return view('patient.dashboard');
        else
            return redirect()->route('patient.continue');
    }
}
