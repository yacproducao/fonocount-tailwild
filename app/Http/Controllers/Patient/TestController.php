<?php

namespace App\Http\Controllers\Patient;

use App\Models\Test;
use App\Models\Concordance;
use App\Models\Patient;
use App\Models\Word;
use App\Models\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $patient = auth()->user()->patient;
        $tests = ($request->has('action') && $request->action == 'filter') ?
            $patient->tests()->filter($request)->paginate(20) :
            $patient->tests()->paginate(20);
        $data = ['tests' => $tests];
        return view('patient.test.index', array_merge($data, $request->only(['concordance_id', 'action'])));
    }
    public function search(Request $request)
    {

        if ($request->concordance_id == null)
            $tests = Test::paginate(20);
        else
            $tests = Test::where('concordance_id', $request->concordance_id)->paginate(20);
        return view('patient.test.index', ['tests' => $tests, 'concordance_id' => $request->concordance_id]);
    }
    public function newTest()
    {

        $concordances = Concordance::all();
        return view('patient.test.new', compact('concordances'));
    }
    public function new(Request $request, $concordance)
    {

        if ($concordance == "congruencia") {
            $concordancia = Concordance::where('name', 'Congruências')->first();
        } else {
            $concordancia = Concordance::where('name', 'Incongruências')->first();
        }
        $patient = auth()->user()->patient;
        $test = new Test();
        $test->concordance()->associate($concordancia);
        $test->patient()->associate($patient);
        $test->save();
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Teste Inicializado " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $test->log()->save($log);
        return redirect()->route('patient.test.create', ['concordance' => $concordance, 'test' => $test]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($concordance, Test $test)
    {

        $teste = Test::find($test->id)->words()->get()->pluck('id')->toArray();
        if (isset($teste[0]))
            $words = Word::whereNotIn('id', $teste)->groupby('type_id')->get();
        else
            $words = Word::groupby('type_id')->get();

        $random = rand(1, 2);
        $cor[0] = $random == 1 ? 'bg-blue-500 hover:bg-blue-200 text-white hover:text-blue-500' : 'bg-red-500 hover:bg-red-200 text-white hover:text-red-500';
        $cor[1] = $cor[0] == 'bg-blue-500 hover:bg-blue-200 text-white hover:text-blue-500' ? 'bg-red-500 hover:bg-red-200 text-white hover:text-red-500' : 'bg-blue-500 hover:bg-blue-200 text-white hover:text-blue-500';

        if ($words->count() > 1)
            return view('patient.test.create', ['words' => $words, 'test' => $test->id, 'cor' => $cor]);
        else
            return redirect()->route('patient.test.show', ['test' => $test->id, 'concordance' => $concordance]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Test $test, Request $request)
    {
        if ($test->concordance->name == "Congruências")
            $concordance = "congruencia";
        else
            $concordance = "incongruencia";
        $words = [
            $request->word => [
                'response' => true,
                'time' => $request->time
            ],
            $request->wordnot => [
                'response' => false,
                'time' => $request->time
            ]
        ];
        $test->words()->sync($words, false);
        return redirect()->route('patient.test.create', ['concordance' => $concordance, 'test' => $test]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $concordance, Test $test)
    {

        if ($concordance == "congruencia") {
            $concordancia = Concordance::where('name', 'Congruências')->first();
        } else {
            $concordancia = Concordance::where('name', 'Incongruências')->first();
        }
        if ($test->concordance_id == $concordancia->id) {
            if (!$test->status) {
                $test->status = true;
                $test->save();
                $log = new Log();
                $log->ip = $request->ip();
                $log->url = $request->url();
                $log->event = "Teste Finalizado " . auth()->user()->id;
                $log->user_id = auth()->user()->id;
                $test->log()->save($log);
            }
            $types = $test->words()->select('type_id', DB::raw('count(id) as contador'))->where('test_word.response', true)->groupby('type_id')->get();
            $time[0] = $test->words()->select('test_word.time')->where('test_word.response', true)->orderby('test_word.time', 'asc')->first()->time;
            $time[1] = $test->words()->select('test_word.time')->where('test_word.response', true)->orderby('test_word.time', 'desc')->first()->time;

            $random = rand(1, 2);
            $i = 0;
            $coranterior = $random == 1 ? 'text-blue' : 'text-red';
            foreach ($types as $type) {
                $values[$i] = new \stdClass;
                $values[$i]->contador = $type->contador;
                $values[$i]->name = $type->type->name;
                $values[$i]->cor = $coranterior;
                $coranterior = $coranterior == 'text-blue' ? 'text-red' : 'text-blue';
                $i++;
            }
            return view('patient.test.result', ['time' => $time, 'types' => $values, 'create' => $test->created_at, 'concordance' => $concordancia]);
        } else {
            return redirect()->route('patient.test.show', ['test' => $test->id, 'concordance' => $concordancia->id == 1 ? "incongruencia" : "congruencia"]);
        }
    }
}
