<?php

namespace App\Http\Controllers\Patient;

use App\Models\Log;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\Avatar;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\AvatarRequest;
use App\Http\Requests\CompleteUserRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Models\Address;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('patient.profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        $user = User::find(auth()->user()->id);
        $user->update($request->except(['_token', '_method', 'Registrar', 'birth', 'sex', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones']));
        $patient = $user->patient;
        $patient->update($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones']));
        $telefone = preg_replace("/[^0-9]/", "", $request->phones);
        $patient->phones =  [
            [
                'ddd' => preg_replace('/\A.{2}?\K[\d]+/', '', $telefone),
                'number' => preg_replace('/^\d{2}/', '', $telefone)
            ]
        ];
        $patient->save();
        if ($request->state_id != null) {
            if ($patient->address) {
                $patient->address->update($request->except(['_token', '_method', 'Registrar', 'name', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'user', 'email']));
                $address = $patient->address;
            } else
                $address = new Address($request->except(['_token', '_method', 'Registrar', 'name', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'user', 'email']));
            $state = State::find($request->state_id);
            $city = City::find($request->city_id);
            $address->state()->associate($state);
            $address->city()->associate($city);
            $patient->address()->save($address);
        }
        if ($request->has('avatar')) {
            if (auth()->user()->avatar) {
                $caminho = realpath(public_path('/storage/avatar/' . auth()->user()->avatar->filename));
                if ($caminho)
                    unlink($caminho);
                $user->avatar()->delete();
            }
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('avatar'));
            $extension = image_type_to_extension($imgdata[2], false);
            $path = Storage::disk('public')->putFileAs('avatar/', $request->file('avatar'), "{$uuid}.{$extension}");
            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Atualização da foto de perfil do usuario " . $user->id;
            $log->user_id = auth()->user()->id;
            $user->log()->save($log);
            $avatar = new Avatar;
            $avatar->filename = pathinfo($path)['basename'];
            $avatar->dimensions = ['width' => $imgdata[0], 'height' => $imgdata[1]];
            $avatar->format = image_type_to_mime_type($imgdata[2]);
            $user->avatar()->save($avatar);
        }
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Editou Perfil " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);
        return back()->withStatus(__('Perfil Editado com Sucesso.'));
    }

    public function complete()
    {
        return view('patient.profile.complete');
    }
    public function completeUser(CompleteUserRequest $request)
    {
        $user = User::find(auth()->user()->id);
        $patient = $user->patient;
        $patient->update($request->except(['_token', '_method', 'Registrar', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones']));
        $telefone = preg_replace("/[^0-9]/", "", $request->phones);
        $patient->phones =  [
            [
                'ddd' => preg_replace('/\A.{2}?\K[\d]+/', '', $telefone),
                'number' => preg_replace('/^\d{2}/', '', $telefone)
            ]
        ];
        $patient->save();
        if ($request->state_id != null) {
            $address = new Address($request->except(['_token', '_method', 'Registrar', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'avatar']));
            $state = State::find($request->state_id);
            $city = City::find($request->city_id);
            $address->state()->associate($state);
            $address->city()->associate($city);
            $patient->address()->save($address);
        }
        if ($request->has('avatar')) {
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('avatar'));
            $extension = image_type_to_extension($imgdata[2], false);
            $path = Storage::disk('public')->putFileAs('avatar/', $request->file('avatar'), "{$uuid}.{$extension}");

            $avatar = new Avatar;
            $avatar->filename = pathinfo($path)['basename'];
            $avatar->dimensions = ['width' => $imgdata[0], 'height' => $imgdata[1]];
            $avatar->format = image_type_to_mime_type($imgdata[2]);
            $user->avatar()->save($avatar);
        }
        $user->active = true;
        $user->save();
        return redirect()->route('patient.home');
    }
    public function password(PasswordRequest $request)
    {
        $user = User::find(auth()->user()->id);
        $user->update(['password' => Hash::make($request->get('password'))]);

        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Senha Alterada " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);
        return back()->withPasswordStatus(__('Senha Editado com Sucesso.'));
    }
}
