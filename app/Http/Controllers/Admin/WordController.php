<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Word;
use App\Models\Type;
use App\Models\Log;
use Illuminate\Http\Request;

class WordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $words = ($request->has('action') && $request->action == 'filter') ?
            Word::filter($request)->paginate(20) :
            Word::paginate(20);
        $data = ['words' => $words, 'i' => 1];
        return view('admin.word.index', array_merge($data, $request->only(['name', 'type_id', 'action'])));
    }
    public function search(Request $request)
    {
        $words = Word::where('name', 'like', '%' . $request->name . '%');
        if ($request->type_id != '')
            $words = $words->where('type_id', $request->type_id);

        return view('admin.word.index', ['words' => $words->paginate(20), 'i' => 1, 'name' => $request->name, 'type_id' => $request->type_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.word.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = Type::find($request->type_id);
        if (str_contains(strtolower($request->name), strtolower(trim($type->name)))) {
            $word = new Word($request->except(['_token', '_method', 'Registrar', 'type_id']));
            $word->type()->associate($type);
            $word->save();

            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Palavra criada " . $word->id;
            $log->user_id = auth()->user()->id;
            $word->log()->save($log);

            return redirect()->route('admin.word.index')->withStatus('Palavra adicionado com sucesso.');
        } else {
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'name' => 'Palavra deve conter o tipo ' . $type->name,
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function show(Word $word)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function edit(Word $word)
    {
        return view('admin.word.edit', compact('word'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Word $word)
    {
        $type = Type::find($request->type_id);
        if (str_contains(strtolower($request->name), strtolower(trim($type->name)))) {
            $word->type()->associate($type);
            $word->update($request->except(['_token', '_method', 'Registrar', 'type_id']));
            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Palavra editada " . $word->id;
            $log->user_id = auth()->user()->id;
            $word->log()->save($log);
            return redirect()->route('admin.word.index')->withStatus('Palavra editada com sucesso.');
        } else {
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'name' => 'Palavra deve conter o tipo ' . $type->name,
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Word $word)
    {
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Palavra deletada " . $word->id;
        $log->user_id = auth()->user()->id;
        $word->log()->save($log);
        $word->delete();
        return redirect()->route('admin.word.index')->withStatus('Palavra deletada com sucesso.');
    }
}
