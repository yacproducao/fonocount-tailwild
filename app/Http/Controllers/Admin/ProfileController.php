<?php

namespace App\Http\Controllers\Admin;

use App\Models\Log;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\Avatar;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileAdminRequest;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('admin.profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileAdminRequest $request)
    {
        $user = User::find(auth()->user()->id);
        $user->update($request->except(['_token', '_method', 'Registrar', 'birth', 'sex', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones']));
        
        if ($request->has('avatar')) {
            if (auth()->user()->avatar) {
                $caminho = realpath(public_path('/storage/avatar/' . auth()->user()->avatar->filename));
                if ($caminho)
                    unlink($caminho);
                $user->avatar()->delete();
            }
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('avatar'));
            $extension = image_type_to_extension($imgdata[2], false);
            $path = Storage::disk('public')->putFileAs('avatar/', $request->file('avatar'), "{$uuid}.{$extension}");
            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Atualização da foto de perfil do usuario " . $user->id;
            $log->user_id = auth()->user()->id;
            $user->log()->save($log);
            $avatar = new Avatar;
            $avatar->filename = pathinfo($path)['basename'];
            $avatar->dimensions = ['width' => $imgdata[0], 'height' => $imgdata[1]];
            $avatar->format = image_type_to_mime_type($imgdata[2]);
            $user->avatar()->save($avatar);
        }
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Editou Perfil " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);
        return back()->withStatus(__('Perfil Editado com Sucesso.'));
    }

    
    public function password(PasswordRequest $request)
    {
        $user = User::find(auth()->user()->id);
        $user->update(['password' => Hash::make($request->get('password'))]);

        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Senha Alterada " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);
        return back()->withStatus(__('Senha Editado com Sucesso.'));
    }
}
