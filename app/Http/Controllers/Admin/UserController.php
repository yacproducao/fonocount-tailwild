<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;
use App\Models\User;
use App\Models\Role;
use App\Models\Address;
use App\Models\Avatar;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use App\Models\State;
use App\Models\City;
use App\Models\Patient;
use App\Models\Log;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = ($request->has('action') && $request->action == 'filter') ?
            User::filter($request)->paginate(20) :
            User::paginate(20);
        $data = ['users' => $users];
        return view('admin.users.index', array_merge($data, $request->only(['role_id', 'name', 'email', 'action'])));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Userrequest $request)
    {
        $role = Role::find($request->role_id);
        $user = new User($request->except(['_token', '_method', 'Registrar', 'birth', 'sex', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id', 'password', 'password_confirmation']));
        $user->role()->associate($role);
        $user->password = Hash::make($request->password);
        $user->active = true;
        $user->save();

        if ($role->name == 'Paciente') {
            $patient = new Patient($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id', 'password', 'password_confirmation']));
            $telefone = preg_replace("/[^0-9]/", "", $request->phones);
            $patient->phones =  [
                [
                    'ddd' => preg_replace('/\A.{2}?\K[\d]+/', '', $telefone),
                    'number' => preg_replace('/^\d{2}/', '', $telefone)
                ]
            ];
            $patient->user()->associate($user);
            $patient->save();
            if ($request->state_id != null) {
                $address = new Address($request->except(['_token', '_method', 'Registrar', 'name', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'user', 'email', 'avatar', 'role_id', 'password', 'password_confirmation']));
                $state = State::find($request->state_id);
                $city = City::find($request->city_id);
                $address->state()->associate($state);
                if ($city)
                    $address->city()->associate($city);
                $patient->address()->save($address);
            }
        }
        if ($request->has('avatar')) {
            $uuid = Uuid::uuid4()->getHex();
            $imgdata = getimagesize($request->file('avatar'));
            $extension = image_type_to_extension($imgdata[2], false);
            $path = Storage::disk('public')->putFileAs('avatar/', $request->file('avatar'), "{$uuid}.{$extension}");

            $avatar = new Avatar;
            $avatar->filename = pathinfo($path)['basename'];
            $avatar->dimensions = ['width' => $imgdata[0], 'height' => $imgdata[1]];
            $avatar->format = image_type_to_mime_type($imgdata[2]);
            $user->avatar()->save($avatar);
        }
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Usuario criada " . $user->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);

        return redirect()->route('admin.user.index')->withStatus('Usuario adicionado com sucesso.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $email = User::where('email', $request->email)->first();
        $usuario = User::where('user', $request->user)->first();
        if ($user->email != $request->email && $email)
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'email' => 'Email já existe',
                ]);
        else if ($user->user != $request->user && $usuario)
            return back()
                ->withInput($request->all())
                ->withErrors([
                    'email' => 'Email já existe',
                ]);
        else {
            $role = Role::find($request->role_id);
            $user->role()->associate($role);
            $user->update($request->except(['_token', '_method', 'Registrar', 'birth', 'sex', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id']));
            if ($request->password != "")
                $user->update(['password' => Hash::make($request->get('password'))]);

            if ($role->name == 'Paciente') {
                if ($user->patient) {
                    $patient = $user->patient;
                    $patient->update($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id']));
                } else if (Patient::onlyTrashed()->where('user_id', $user->id)->first()) {
                    $patient = Patient::onlyTrashed()->where('user_id', $user->id)->first();
                    $patient->update($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id']));
                    $patient->restore();
                } else
                    $patient = new Patient($request->except(['_token', '_method', 'Registrar', 'email', 'user', 'zip_code', 'street', 'number', 'district', 'extra', 'state_id', 'avatar', 'city_id', 'phones', 'role_id']));
                $telefone = preg_replace("/[^0-9]/", "", $request->phones);
                $patient->phones =  [
                    [
                        'ddd' => preg_replace('/\A.{2}?\K[\d]+/', '', $telefone),
                        'number' => preg_replace('/^\d{2}/', '', $telefone)
                    ]
                ];
                $patient->user()->associate($user);
                $patient->save();
                if ($request->state_id != "") {
                    if ($patient->address) {
                        $address = $patient->address;
                        $address->update($request->except(['_token', '_method', 'Registrar', 'name', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'user', 'email', 'avatar', 'role_id']));
                    } else {
                        $address = new Address($request->except(['_token', '_method', 'Registrar', 'name', 'birth', 'sex', 'phones', 'state_id', 'city_id', 'user', 'email', 'avatar', 'role_id']));
                    }
                    $state = State::find($request->state_id);
                    $city = City::find($request->city_id);
                    $address->state()->associate($state);
                    if ($city)
                        $address->city()->associate($city);
                    $patient->address()->save($address);
                } else if ($patient->address) {
                    $patient->address()->delete();
                }
            } else if ($user->patient) {
                $user->patient()->delete();
            }
            if ($request->has('avatar')) {
                if ($user->avatar) {
                    $caminho = realpath(public_path('/storage/avatar/' . $user->avatar->filename));
                    if ($caminho)
                        unlink($caminho);
                    $user->avatar()->delete();
                }
                $uuid = Uuid::uuid4()->getHex();
                $imgdata = getimagesize($request->file('avatar'));
                $extension = image_type_to_extension($imgdata[2], false);
                $path = Storage::disk('public')->putFileAs('avatar/', $request->file('avatar'), "{$uuid}.{$extension}");

                $avatar = new Avatar;
                $avatar->filename = pathinfo($path)['basename'];
                $avatar->dimensions = ['width' => $imgdata[0], 'height' => $imgdata[1]];
                $avatar->format = image_type_to_mime_type($imgdata[2]);
                $user->avatar()->save($avatar);
            }
            $log = new Log();
            $log->ip = $request->ip();
            $log->url = $request->url();
            $log->event = "Usuario Atualizado " . $user->id;
            $log->user_id = auth()->user()->id;
            $user->log()->save($log);

            return redirect()->route('admin.user.edit', ['user' => $user])->withStatus('Usuario atualizado com sucesso.');
        }
    }
    public function password(PasswordRequest $request, User $user)
    {
        $user->update(['password' => Hash::make($request->get('password'))]);

        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Senha Alterada " . auth()->user()->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);
        return back()->withStatus(__('Senha Editado com Sucesso.'));
    }
    public function destroy(Request $request, User $user)
    {
        if ($user->patient) {
            $user->patient()->delete();
        }
        $user->delete();
        $log = new Log();
        $log->ip = $request->ip();
        $log->url = $request->url();
        $log->event = "Usuario Deletado " . $user->id;
        $log->user_id = auth()->user()->id;
        $user->log()->save($log);

        return redirect()->route('admin.user.index')->withStatus('Usuario deletado com sucesso.');
    }
}
