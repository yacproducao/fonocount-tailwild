<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Word;
use App\Models\Patient;
use App\Models\Test;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index()
    {
        $start = Carbon::now()->startOfMonth()->toDateString();
        $end = Carbon::now()->endOfMonth()->toDateString();
        $startAnterior = Carbon::parse($start)->subMonth(1)->startOfMonth()->toDateString();
        $endAnterior = Carbon::parse($start)->subMonth(1)->endOfMonth()->toDateString();
        $patient = Patient::whereBetween('created_at', [$start, $end])->get()->count();
        $patientAnterior = Patient::whereBetween('created_at', [$startAnterior, $endAnterior])->get()->count();
        if ($patient == 0) $atual = 1;
        else $atual = $patient;
        if ($patientAnterior == 0) $patientAnterior = 1;
        $percentual = ($atual - $patientAnterior) / $patientAnterior;
        $comparativopatient = $percentual * 100;
        $words = Word::whereBetween('created_at', [$start, $end])->get()->count();
        $wordsAnterior = Word::whereBetween('created_at', [$startAnterior, $endAnterior])->get()->count();
        if ($words == 0) $atual = 1;
        else $atual = $words;
        if ($wordsAnterior == 0) $wordsAnterior = 1;
        $percentual = ($atual - $wordsAnterior) / $wordsAnterior;
        $comparativoword = $percentual * 100;
        $test = Test::whereBetween('created_at', [$start, $end])->get()->count();
        $testAnterior = Test::whereBetween('created_at', [$startAnterior, $endAnterior])->get()->count();
        if ($test == 0) $atual = 1;
        else $atual = $test;
        if ($testAnterior == 0) $testAnterior = 1;
        $percentual = ($atual - $testAnterior) / $testAnterior;
        $comparativotest = $percentual * 100;
        $data = [
            'patient' => $patient,
            'comparativopatient' => $comparativopatient,
            'comparativotest' => $comparativotest,
            'comparativoword' => $comparativoword,
            'test' => $test,
            'word' => $words,
        ];
        return view('admin.dashboard', $data);
    }
}
