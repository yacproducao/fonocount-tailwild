<?php

namespace App\Http\Controllers\Admin;

use App\Models\Test;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PdfviewController extends Controller
{
    public function index(Test $test){

        $types = $test->words()->select('type_id', DB::raw('count(id) as contador'))->where('test_word.response', true)->groupby('type_id')->get();
        $time[0] = $test->words()->select('test_word.time')->where('test_word.response', true)->orderby('test_word.time', 'asc')->first()->time;
        $time[1] = $test->words()->select('test_word.time')->where('test_word.response', true)->orderby('test_word.time', 'desc')->first()->time;

        $random = rand(1, 2);
        $i = 0;
        $coranterior = $random == 1 ? 'blue' : 'red';
        foreach ($types as $type) {
            $values[$i] = new \stdClass;
            $values[$i]->contador = $type->contador;
            $values[$i]->name = $type->type->name;
            $values[$i]->cor = $coranterior;
            $coranterior = $coranterior == 'blue' ? 'red' : 'blue';
            $i++;
        }
        return PDF::loadView('admin.pdf.view',['test'=>$test, 'time' => $time, 'types' => $values])
        ->stream();
    }
}
