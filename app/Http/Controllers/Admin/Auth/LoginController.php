<?php

namespace App\Http\Controllers\Admin\Auth;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = "/admin/";

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	public function login(Request $request)
	{
		try{
			if(Auth::attempt($request->only(['email','password']), $request->has('remember'))){
				return redirect()->route('admin.home');
			} else
			return back()
				->withInput($request->only('email', 'remember'))
				->withErrors([
					'email' => 'As credenciais informadas não são válidas',
				]);

		} catch (ModelNotFoundException $e) {
			return back()
			->withInput($request->only('email', 'remember'))
			->withErrors([
				'email' => 'As credenciais informadas não são válidas',
			]);
		}
	}

	public function logout(Request $request)
	{
		Auth::logout();
		return redirect()->route('admin.home');
	}

	public function showLoginForm()
	{
		if(Auth::user())
			return redirect()->route('admin.home');
		else
			return view('admin.auth.login');
	}
}
