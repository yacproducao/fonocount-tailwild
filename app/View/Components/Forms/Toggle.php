<?php

namespace App\View\Components\Forms;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class Toggle extends Component
{
	public $id;
	public $name;
	public $caption;
	public $state;
	protected $labelRight;
	public $labelClasses;

    public function __construct(bool $state = false,string $name = null, string $caption = null, bool $labelRight = false, string $classes='', string $id=null)
    {
		$this->id = $id ?? 'tgl-' . Str::random(16);
		$this->name = $name;
		$this->caption = $caption;
		$this->state = $state;
		$this->labelRight = $labelRight;

		$this->labelClasses = array_unique(array_merge([
			'flex-grow',
			$labelRight ? 'pl-2 order-2' : 'pr-2'
		],preg_split('/\s/', $classes)));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.toggle');
    }
}
