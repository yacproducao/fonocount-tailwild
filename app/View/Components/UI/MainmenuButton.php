<?php

namespace App\View\Components\UI;

use Illuminate\View\Component;

class MainmenuButton extends Component
{
	public $icon;
	public $url;
	public $classes;

    public function __construct(string|array $icon, string $url, array $sections=null)
    {
        $this->icon = is_array($icon) ? join(' ', array_unique( $icon )) : $icon;
		$this->url = route($url);
		if($sections)
			$this->classes = request()->routeIs($sections)? ['link', 'active']:['link'];
		else
			$this->classes = request()->routeIs(preg_replace("/\w+$/", "*", $url))? ['link', 'active']:['link'];
		//request()->is(isset($sections) ? ...$sections : preg_replace('/^\//','',$url)) ? 'link active' : 'link'
    }

    public function render()
    {
        return view('components.u-i.mainmenu-button');
    }
}
