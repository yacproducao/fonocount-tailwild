<?php

namespace App\View\Components\UI;

use Illuminate\View\Component;

class Symbol extends Component
{
	public $width;
	public $classes;

	public function __construct(float $width = 800, string|array $classes = ['fill-current', 'text-black'])
	{
		if (!is_array($classes)) {
			$classes = preg_split('/\s/', $classes);
		}
		$this->classes = join(' ', array_unique(array_merge(['fill-current'], $classes)));
		$this->width = $width;
	}

	public function render()
	{
		return view('components.u-i.symbol');
	}
}
