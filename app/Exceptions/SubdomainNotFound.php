<?php

namespace App\Exceptions;

use Exception;

use Illuminate\Http\Request;

class SubdomainNotFound extends Exception
{
    protected $message="Subdominio não encontrada";
    protected $longText="Este subdomínio não se encontra cadastrado em nosso sistema, por favor, verifique a url informada e tente novamente";
	protected $code=404;

	public function render(Request $request)
	{
		return response()->view('errors.subdominionotfound', ['message' => $this->message, "text"=>$this->longText], $this->code);
	}
}