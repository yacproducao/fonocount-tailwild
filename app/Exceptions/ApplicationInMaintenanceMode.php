<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

class ApplicationInMaintenanceMode extends Exception
{
	protected $statusCode;
	protected $message;
	protected $headers;

	public function __construct(int $statusCode, ?string $message = '', \Throwable $previous = null, array $headers = [], ?int $code = 0)
	{
		if (null === $message) {
			trigger_deprecation('symfony/http-kernel', '5.3', 'Passing null as $message to "%s()" is deprecated, pass an empty string instead.', __METHOD__);

			$message = '';
		}
		if (null === $code) {
			trigger_deprecation('symfony/http-kernel', '5.3', 'Passing null as $code to "%s()" is deprecated, pass 0 instead.', __METHOD__);

			$code = 0;
		}

		$this->statusCode = $statusCode;
		$this->headers = $headers;

		parent::__construct($message, $statusCode, $previous);
	}

	public function getStatusCode()
	{
		return $this->statusCode;
	}

	public function getHeaders()
	{
		return $this->headers;
	}

	public function setHeaders(array $headers)
	{
		$this->headers = $headers;
	}

	public function render(Request $request)
	{
		return response()->view('errors.maintenance', ['message' => $this->message, 'headers'=>$this->headers], $this->statusCode);
	}
}
