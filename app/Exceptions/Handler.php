<?php

namespace App\Exceptions;

use Illuminate\Support\Facades\Route;
use App\Exceptions\SubdomainNotFound;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
    }

    public function render($request, Throwable $e)
    {
        $subdomain = 'paciente.' . $request->url();
        if ($e instanceof NotFoundHttpException && isset($subdomain)) {
            return parent::render($request, new SubdomainNotFound());
        }
        if ($e instanceof NotFoundHttpException && isset($subdomain)) {
            $new = new SubdomainNotFound;
            return $new->render($request);
        }
        return parent::render($request, $e);
    }
}
