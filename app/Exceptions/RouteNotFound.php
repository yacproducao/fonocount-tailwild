<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;


class RouteNotFound extends Exception
{
    
	protected $message = "Página não encontrada";
	protected $code = 404;

	public function render(Request $request)
	{
		return response()->view('errors.404', ['message' => $this->message], $this->code);
	}
}