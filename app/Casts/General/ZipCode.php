<?php

namespace App\Casts\General;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class ZipCode implements CastsAttributes {
	public function get($model, $key, $value, $attributes)
	{
		return preg_replace('/(\d{5})(\d{3})/','$1-$2',$value);
	}

	public function set($model, $key, $value, $attributes)
	{
		return $value!=''?preg_replace('/\D/','',$value):null;
	}
}
