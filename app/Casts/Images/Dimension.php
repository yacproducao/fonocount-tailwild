<?php

namespace App\Casts\Images;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Dimension implements CastsAttributes {
	public function get($model, $key, $value, $attributes)
	{
		//return preg_replace('/(\d+)x(\d+)/','$1.$2.$3-$4',$value);
		$arr = preg_split('/x/',$value);
		return [
			'width'=>$arr[0],
			'height'=>$arr[1],
		];
	}

	public function set($model, $key, $value, $attributes)
	{
		return join('x',$value);
	}
}
