<?php

namespace App\Casts\Numbers;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Decimal implements CastsAttributes
{
	protected $digits;
	protected $decimal;

	public function __construct($digits = 8, $decimal = 2) {
		$this->digits = $digits;
		$this->decimal = $decimal;
	}
	public function get($model, $key, $value, $attributes)
	{
		$num = \NumberFormatter::create(config('app.locale'), \NumberFormatter::DECIMAL);
		return $num->format($value);
	}

	public function set($model, $key, $value, $attributes)
	{
		return $value;
	}
}
