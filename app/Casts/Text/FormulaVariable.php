<?php

namespace App\Casts\Text;

use Illuminate\Support\Str;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class FormulaVariable implements CastsAttributes
{
	public function get($model, $key, $value, $attributes)
	{
		return $value;
	}

	public function set($model, $key, $value, $attributes)
	{
		/*$slug = $value?Str::Camel(preg_replace('/-/', '_', Str::slug($value))):null;
		preg_match('/\d+/',$slug,$matches);*/
		return $value?Str::Camel(preg_replace('/-/', '_', Str::slug($value))):null;
	}
}
