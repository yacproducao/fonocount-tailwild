<?php

namespace App\Traits;

use Ramsey\Uuid\Provider\Node\StaticNodeProvider;
use Ramsey\Uuid\Type\Hexadecimal;
use Ramsey\Uuid\Uuid;

trait Uuids
{
	protected static function boot()
	{
		parent::boot();
		$nodeProvider = new StaticNodeProvider(new Hexadecimal(env('APP_UUID_NODEPROVIDER')));

		static::creating(function($model) use($nodeProvider){
			if (empty($model->{$model->getKeyName()})) {
				$model->{$model->getKeyName()} = Uuid::uuid6($nodeProvider->getNode())->toString();
			}
		});
	}

	public function getIncrementing()
	{
		return false;
	}

	public function getKeyType()
	{
		return 'string';
	}
}
