<?php

namespace App\Traits;

use Ramsey\Uuid\Provider\Node\StaticNodeProvider;
use Ramsey\Uuid\Type\Hexadecimal;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Relations\Relation;

trait HasUuidChildModel
{
	public function newFromBuilder($attributes = [], $connection = null)
	{
		$morphMap = Relation::morphMap();
		$entryType = Arr::get((array)$attributes, 'type');
		if (class_exists($morphMap[$entryType]) && is_subclass_of($morphMap[$entryType], self::class)) {
			$model = new $morphMap[$entryType];
		} else {
			$model = $this->newInstance();
		}

		$model->exists = true;
		$model->setRawAttributes((array) $attributes, true);
		$model->setConnection($connection ?? $this->connection);

		return $model;
	}

	protected static function boot()
	{
		parent::boot();
		$nodeProvider = new StaticNodeProvider(new Hexadecimal(env('APP_UUID_NODEPROVIDER')));

		static::creating(function ($model) use ($nodeProvider) {
			if (empty($model->{$model->getKeyName()})) {
				$morphMap = array_flip(Relation::morphMap());
				$model->{$model->getKeyName()} = Uuid::uuid6($nodeProvider->getNode())->toString();
				$model->type = $morphMap[get_called_class()];
			}
		});
	}

	public function getIncrementing()
	{
		return false;
	}

	public function getKeyType()
	{
		return 'string';
	}
}
