<?php

namespace App\Observers;

use App\Models\Newsletter;
use Illuminate\Support\Str;

class NewsletterObserver
{
	public function creating(Newsletter $newsletter)
	{
		if(!$newsletter->slug)
			$newsletter->slug = Str::slug($newsletter->name);
	}

    public function created(Newsletter $newsletter)
    {
        //
    }
    public function updated(Newsletter $newsletter)
    {
        //
    }

    public function deleted(Newsletter $newsletter)
    {
        //
    }
    public function restored(Newsletter $newsletter)
    {
        //
    }

    public function forceDeleted(Newsletter $newsletter)
    {
        //
    }
}
