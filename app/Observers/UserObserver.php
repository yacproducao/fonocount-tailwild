<?php

namespace App\Observers;

use App\Models\User;
use Ramsey\Uuid\Uuid;

class UserObserver
{
	public function creating(User $user)
	{
		if(!$user->api_token)
			$user->api_token = Uuid::uuid5(env('APP_UUID_DOMAIN'), $user->email ?? $user->name);
	}

	public function created(User $user)
	{
		//
	}

	public function updated(User $user)
	{
		//
	}

	public function deleted(User $user)
	{
		//
	}

	public function restored(User $user)
	{
		//
	}

	public function forceDeleted(User $user)
	{
		//
	}
}
