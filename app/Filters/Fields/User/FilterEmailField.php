<?php

namespace App\Filters\Fields\User;

use App\Filters\Fields\FieldFilter;

class FilterEmailField extends FieldFilter{

	protected $field = 'users.email';
	protected $operator = 'like';

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder
			->where($this->field, $this->operator, preg_replace('/\*/', '%', preg_replace('/\*/', '%', $value)));
			
	}

}
