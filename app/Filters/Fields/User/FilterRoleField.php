<?php

namespace App\Filters\Fields\User;

use App\Filters\Fields\FieldFilter;

class FilterRoleField extends FieldFilter{

	protected $field = 'role_id';
	protected $operator = '=';

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder->where($this->field, $this->operator, preg_replace('/\*/', '', $value));
	}

}
