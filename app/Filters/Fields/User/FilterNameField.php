<?php

namespace App\Filters\Fields\User;

use Illuminate\Support\Str;
use App\Filters\Fields\FieldFilter;

class FilterNameField extends FieldFilter
{

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder->where(function($qry) use($value){
			$qry
				->where('users.name', 'like', preg_replace('/\*/', '%', $value));
		});
	}
}
