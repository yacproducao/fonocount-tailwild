<?php

namespace App\Filters\Fields;

class FilterCNPJField extends FieldFilter{

	protected $field = 'cnpj';
	protected $operator = 'like';

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder->where($this->field, $this->operator, preg_replace('/\*/', '%', preg_replace('/\*/', '%', preg_replace('/[^\d\*]/','',$value))));
	}

}
