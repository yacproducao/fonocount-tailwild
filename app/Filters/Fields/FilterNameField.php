<?php

namespace App\Filters\Fields;

class FilterNameField extends FieldFilter {

	protected $field = 'name';
	protected $operator = 'like';

}
