<?php

namespace App\Filters\Fields;

class FieldFilter {

	protected $field = 'name';
	protected $operator = 'like';

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder->where($this->field, $this->operator, preg_replace('/\*/', '%', $value));
	}

}
