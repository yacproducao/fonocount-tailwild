<?php

namespace App\Filters\Fields\Test;

use App\Filters\Fields\FieldFilter;

class FilterPatientField extends FieldFilter{

	protected $field = 'patient_id';
	protected $operator = '=';

	public function filter($builder, $value)
	{
		$value = preg_replace('/\s/', '*', $value);
		if (!preg_match('/\*/', $value))
			$value .= "*";
		return $builder
			->where($this->field, $this->operator, preg_replace('/\*/', '', $value));
	}

}
