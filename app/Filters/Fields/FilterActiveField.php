<?php

namespace App\Filters\Fields;

class FilterActiveField extends FieldFilter{
	protected $field = 'active';
	protected $operator = '=';
}
