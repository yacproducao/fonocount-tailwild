<?php

namespace App\Filters;

use App\Filters\Fields\Word\FilterNameField;
use App\Filters\Fields\Word\FilterTypeField;

class WordFilter extends Filter{
	protected $filters = [
		'name' => FilterNameField::class,
		'type_id' => FilterTypeField::class
	];
}
