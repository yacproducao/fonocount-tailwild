<?php

namespace App\Filters;

use App\Filters\Fields\Test\FilterConcordanceField;
use App\Filters\Fields\Test\FilterPatientField;

class TestFilter extends Filter{
	protected $filters = [
		'concordance_id' => FilterConcordanceField::class,
		'patient_id' => FilterPatientField::class
	];
}
