<?php

namespace App\Filters;

use App\Filters\Fields\User\FilterEmailField;
use App\Filters\Fields\User\FilterRoleField;
use App\Filters\Fields\User\FilterNameField;

class UserFilter extends Filter{
	protected $filters = [
		'name' => FilterNameField::class,
		'role_id' => FilterRoleField::class,
		'email' => FilterEmailField::class,
	];
}
