<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class MorphMapServiceProvider extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Relation::morphMap([
			'address' => 'App\Models\Address',
			'avatar' => 'App\Models\Avatar',
			'city' => 'App\Models\City',
			'image' => 'App\Models\Image',
			'log' => 'App\Models\Log',
			'permission' => 'App\Models\Permission',
			'patient' => 'App\Models\Patient',
			'picture' => 'App\Models\Picture',
			'role' => 'App\Models\Role',
			'state' => 'App\Models\State',
			'user' => 'App\Models\User',
			'type' => 'App\Models\Type',
			'word' => 'App\Models\Word',
			'concordance' => 'App\Models\Concordance',
		]);
	}
}
