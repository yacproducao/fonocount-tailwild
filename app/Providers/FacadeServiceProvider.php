<?php

namespace App\Providers;

use Jaybizzle\Seasons;
use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider{
	public function register()
	{
		\App::bind('season', function () {
			return (new Seasons())->southern();
		});
	}
}
