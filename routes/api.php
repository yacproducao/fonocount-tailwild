<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\City;
use App\Models\Role;
use App\Models\Type;
use App\Models\State;
use App\Models\Concordance;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/role', function () {
    return Role::orderBy('name', 'ASC')->get();
});
Route::get('/patient', function () {
    return Patient::orderBy('name', 'ASC')->get();
});
Route::get('/verificauser/{user}/{id?}', function ($usuario, $id = null) {
    if (Auth::check()) {
        if (auth()->user()->user == $usuario) {
            return 2;
        }
    }
    $valor = User::where('user', $usuario)->first();
    if ($valor) {
        if ($valor->id != $id)
            return 1;
        else
            return 2;
    } else if ($usuario == null) {
        return 1;
    } else {
        return 2;
    }
});
Route::get('/verificaemail/{email}/{id?}', function ($email, $id = null) {
    if (Auth::check()) {
        if (auth()->user()->email == $email) {
            return 2;
        }
    }
    $valor = User::where('email', $email)->first();
    if ($valor) {
        if ($valor->id != $id)
            return 1;
        else
            return 2;
    } else if ($email == null) {
        return 1;
    } else {
        return 2;
    }
});

Route::get('/cities/{id?}', function ($state_id = null) {
    if ($state_id)
        return City::where('state_id', $state_id)->orderBy('state_id', 'ASC')->orderBy('name', 'ASC')->get();
    else
        return City::all();
});

Route::get('/states', function () {
    return State::select('states.*')->join('countries', 'countries.id', '=', 'states.country_id')->where('countries.native', 'Brasil')->orderBy('states.name', 'ASC')->get();
});
Route::get('cep/{cep}', function ($cep) {
    try {
        $response = \Canducci\Cep\Facades\Cep::find($cep);
        if ($response->isOk()) {
            $data = $response->getCepModel()->jsonSerialize();
            $state = State::where('uf', $data['uf'])->select('states.*')->join('countries', 'countries.id', '=', 'states.country_id')->where('countries.native', 'Brasil')->first();
            $data = Arr::add($data, 'state', $state->toArray());
            $city = City::where('name', 'like', $data['localidade'])->first();
            $data = Arr::add($data, 'city', $city->toArray());
            return response()->json(Arr::except($data, ['uf', 'localidade']));
        }
        return response()->json(['status' => 'error', 'error' => "CEP \"{$cep}\" não encontrado"]);
    } catch (\Throwable $th) {
        return response()->json(['status' => 'error', 'error' => "CEP \"{$cep}\" inválido", 'msg' => $th->getMessage()]);
    }
});
Route::get('/type', function () {
    return Type::orderBy('name', 'ASC')->get();
});
Route::get('/concordance', function () {
    return Concordance::orderBy('name', 'ASC')->get();
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
