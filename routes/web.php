<?php

use Illuminate\Support\Facades\Route;

Route::group(['domain' => env('APP_DOMAIN')], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::get('cadastro', 'Auth\RegisterController@create')->name('cadastro');
    Route::post('login', 'Auth\LoginController@login')->name('login_entrar');
    Route::post('cadastro', 'Auth\RegisterController@store')->name("cadastrar");
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});
