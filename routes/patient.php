
<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['check_permissions', 'auth'], 'domain' => 'paciente.' . env('APP_DOMAIN')], function () {
	Route::post('logout', 'Auth\LoginController@logout')->name("patient.logout");
	Route::get('/', 'HomeController@index')->name('patient.home');
	Route::get('/perfil', ['as' => 'patient.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::get('/complete', ['as' => 'patient.continue', 'uses' => 'ProfileController@complete']);
	Route::put('/complete/perfil', ['as' => 'patient.profile.completeUser', 'uses' => 'ProfileController@completeUser']);
	Route::put('/perfil', ['as' => 'patient.profile.update', 'uses' => 'ProfileController@update']);
	Route::put('/perfil/senha', ['as' => 'patient.profile.password', 'uses' => 'ProfileController@password']);

	Route::prefix('teste')->group(function () {
		Route::get('/', 'TestController@index')->name('patient.test.index');
		Route::get('/novo', 'TestController@newTest')->name('patient.test.new');
		Route::get('/novo/{concordance}', 'TestController@new')->name('patient.test');
		Route::get('/novo/{concordance}/{test}', 'TestController@create')->name('patient.test.create');
		Route::get('/resultado/{concordance}/{test}', 'TestController@show')->name('patient.test.show');
		Route::put('/pesquisa', 'TestController@search')->name('patient.test.search');
		Route::put('/inserir/{test}', 'TestController@store')->name('patient.test.store');
	});
});
