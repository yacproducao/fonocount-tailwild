<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['check_permissions_patient', 'auth'], 'domain' => env('APP_DOMAIN')], function () {
	Route::post('logout', 'Auth\LoginController@logout')->name("logout");
	Route::put('perfil/avatar', ['as' => 'profile.updateAvatar', 'uses' => 'ProfileController@updateAvatar']);
	Route::get('perfil', ['as' => 'admin.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/perfil', ['as' => 'admin.profile.update', 'uses' => 'ProfileController@update']);
	Route::put('/perfil/senha', ['as' => 'admin.profile.password', 'uses' => 'ProfileController@password']);

	//Admin Routes
	Route::get('/', 'HomeController@index')->name('admin.home');

	Route::prefix('palavras')->group(function () {
		Route::get('/', ['as' => 'admin.word.index', 'uses' => 'WordController@index']);
		Route::get('/adicionar', ['as' => 'admin.word.create', 'uses' => 'WordController@create']);
		Route::get('/editar/{word}', ['as' => 'admin.word.edit', 'uses' => 'WordController@edit']);
		Route::put('/adicionar', ['as' => 'admin.word.store', 'uses' => 'WordController@store']);
		Route::put('/editar/{word}', ['as' => 'admin.word.update', 'uses' => 'WordController@update']);
		Route::delete('/deletar/{word}', ['as' => 'admin.word.destroy', 'uses' => 'WordController@destroy']);
	});
	Route::prefix('teste')->group(function () {
		Route::get('/', 'TestController@index')->name('admin.test.index');
		Route::get('/pdf/{test}', 'PdfviewController@index')->name('admin.test.pdf');
		Route::get('/novo', 'TestController@newTest')->name('admin.test.new');
		Route::get('/novo/criar/{concordance}/{patient}', 'TestController@new')->name('admin.test');
		Route::get('/novo/{concordance}/{test}', 'TestController@create')->name('admin.test.create');
		Route::get('/resultado/{concordance}/{test}', 'TestController@show')->name('admin.test.show');
		Route::put('/inserir/{test}', 'TestController@store')->name('admin.test.store');
	});
	Route::prefix('usuarios')->group(function () {
		Route::get('/', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
		Route::get('/adicionar', ['as' => 'admin.user.create', 'uses' => 'UserController@create']);
		Route::get('/editar/{user}', ['as' => 'admin.user.edit', 'uses' => 'UserController@edit']);
		Route::put('/adicionar', ['as' => 'admin.user.store', 'uses' => 'UserController@store']);
		Route::put('/editar/{user}', ['as' => 'admin.user.update', 'uses' => 'UserController@update']);
		Route::put('/password/{user}', ['as' => 'admin.user.password', 'uses' => 'UserController@password']);
		Route::delete('/deletar/{user}', ['as' => 'admin.user.destroy', 'uses' => 'UserController@destroy']);
	});
	
});
