const
    mix = require('laravel-mix'),
    path = require('path'),
    Dotenv = require('dotenv'),
    fs = require('fs'),
    webpack = require('webpack'),
    tailwindcss = require('tailwindcss');

require ('laravel-mix-purgecss');


const currentPath = path.join(__dirname);
const basePath = currentPath + '/.env';
const envPath = basePath + '.' + (mix.inProduction()?'production':'development');
const finalPath = fs.existsSync(envPath) ? envPath : basePath;

const fileEnv = Dotenv.config({ path: finalPath }).parsed;

const EnvKeys = Object.keys(fileEnv).reduce((prev, next) => {
    let param = [...fileEnv[next].matchAll(/\$\{(?<param>[\w\d_-]+?)\}/g)].reduce((param, next) => {
        return param.replace(next[0], fileEnv[next[1]]);
    }, fileEnv[next]);
    prev[`process.env.${next}`] = JSON.stringify(param);
    return prev;
}, {});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

if(!mix.inProduction())
    mix.sourceMaps();

mix.webpackConfig({
    resolve: {
        alias: {
            contexts: path.resolve(__dirname, "resources/jsx/contexts"),
            common: path.resolve(__dirname, "resources/jsx/common/"),
            components: path.resolve(__dirname, "resources/jsx/components/"),
            sass: path.resolve(__dirname, "resources/jsx/sass"),
        },
    },
    plugins: [new webpack.DefinePlugin(EnvKeys)],
})
    .babelConfig({
        plugins: ["@babel/plugin-proposal-class-properties"],
    })
    .js("resources/js/app.js", "public/js")
    .js('resources/js/script.js', 'public/js')
    .js('resources/js/alpine.js', 'public/js')
    .js('resources/js/gsap-latest-beta.js', 'public/js')
    .js('resources/js/CSSRulePlugin3.js', 'public/js')
    .js('resources/js/alpinecomponent.js', 'public/js')
    .js('resources/js/sliderapp.js', 'public/js')
    //  .js('resources/js/lavaLamp.js', 'public/js')
    //  .js('resources/jsx/StatusList.jsx', 'public/js/status.js')
    .react()
    .extract()
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/layout.scss", "public/css")
    .sass("resources/sass/mainstyle.scss", "public/css")
    .options({
        processCssUrls: false,
        autoprefixer: {
            options: {
                browsers: ["last 6 versions"],
            },
        },
        postCss: [tailwindcss("./tailwind.config.js")],
    })
    .purgeCss({
        enabled: mix.inProduction(),
        content: [
            "resources/**/*.blade.php",
            "resources/**/*.js",
            "resources/**/*.jsx",
        ],
        safelist: {
            deep: [
                /canvas/,
            ],
        },
    })
    .version();