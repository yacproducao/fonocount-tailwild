
# Instalação

Para utilziar este esqueleto, primeiro instale a ferramenta degit, para facilitar a criação de projetos baseados neste esqueleto;
```bash
npm install -g degit
```

Pode ser necessário acesso de super usuário para instalar globalmente, para isto utilize o comando:
```bash
sudo npm install -g degit
```

# Utilização

Após a instalação da ferramenta degit, utilize a linha de comando abaixo para criar projetos com base neste repositório:
```bash
degit --mode=git git@bitbucket.org:yacwebworks/yac-lrvl-tailwind-skeleton.git meu-novo-projeto
```

## Laravel Models

Uma observação à se fazer é que, até que este repositório tenha um comando similar ao `laravel new` alguns passos deverão ser executados manualmente antes do desenvolvimento poder ser iniciado.

O ajuste obrigatório das variáveis APP_UUID_DOMAIN, APP_UUID_NODEPROVIDER, PUSHER_APP_ID, PUSHER_APP_KEY e PUSHER_APP_SECRET precisa ser feita segundo os tipos informados abaixo.


Chave                 | Valor
----------------------| ------
APP_UUID_DOMAIN       | UUID
APP_UUID_NODEPROVIDER | Hexadecimal de 12 dígitos
PUSHER_APP_ID         | UUID
PUSHER_APP_KEY        | UUID
PUSHER_APP_SECRET     | UUID

Após esta configuração, as migrações podem acontecer, assim como o _database seeding_.

### Ids não inteiros

Uma coisa a observar, é que os unique ids dos modelos criados precisam utilizar o Trait Uuids, presente em `app/Traits/Uuids`. E nas migrações, ao invés de:
```php
Schema::create('table', function (Blueprint $table) {
	$table->id();
	.
	.
	.
});
```
O campo ID precisa ser declarado como:
```php
Schema::create('table', function (Blueprint $table) {
	$table->uuid('id')->primary();
	.
	.
	.
});
```

Happy coding!