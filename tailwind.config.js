const colors = require('tailwindcss/colors');
const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
	future: {
		removeDeprecatedGapUtilities: true,
		purgeLayersByDefault: true,
	},
	purge: false,
	darkMode: "media", // or 'media' or 'class'
	theme: {
		fontFamily: {
			sans: "Jost, system-ui,-apple-system, /*`system-ui`, */'Segoe UI', Roboto,	Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji'",
			display: "Big\\ Shoulders\\ Display",
		},
		colors: {
			transparent: "transparent",
			current: "currentColor",
			black: colors.black,
			white: colors.white,
			gray: colors.trueGray,
			red: colors.red,
			orange: colors.orange,
			yellow: colors.amber,
			green: colors.green,
			teal: colors.teal,
			blue: colors.blue,
			indigo: colors.indigo,
			purple: colors.purple,
			pink: colors.pink,
			original: {
				light: "#d0a6cd",
				DEFAULT: "#ad66a6",
				dark: "#742169",
			},
			autumn: {
				light: "#ca9e67",
				DEFAULT: "#936037",
				dark: "#683c11",
			},
			spring: {
				light: "#ea527d",
				DEFAULT: "#bf3e5e",
				dark: "#872b42",
			},
			summer: {
				light: "#f9b233",
				DEFAULT: "#ef7d00",
				dark: "#e94e1b",
			},
			winter: {
				light: "#50c3f1",
				DEFAULT: "#0379bd",
				dark: "#075188",
			},
		},
		minWidth: {
			0: "0",
			8: "8rem",
			16: "1.6rem",
			24: "2.4rem",
			32: "3.2rem",
			64: "6.4rem",
			128: "12.8rem",
			"1/4": "25%",
			"1/2": "50%",
			"3/4": "75%",
			"9/10": "90%",
			avatar: "3.2rem",
			full: "100%",
		},
		minHeight: {
			0: "0",
			8: "8rem",
			16: "1.6rem",
			24: "2.4rem",
			32: "3.2rem",
			64: "6.4rem",
			128: "12.8rem",
			"1/4": "25%",
			"1/2": "50%",
			"3/4": "75%",
			"9/10": "90%",
			avatar: "3.2rem",
			full: "100%",
		},
		extend: {
			gridTemplateRows: {
				soon: "30vh 1fr",
			},
			gridTemplateColumns: {
				soon: "30vw 1fr",
			},
			lineHeight: {
				"extra-loose": "2.5",
			},
		},
	},
	variants: {
		extend: {
			textColor: ["disabled"],
			backgroundColor: ["disabled"],
			backgroundOpacity: ["disabled"],
		},
	},
	plugins: [
		require("@tailwindcss/typography"),
		require("@tailwindcss/forms"),
		require("@tailwindcss/line-clamp"),
		require("@tailwindcss/aspect-ratio"),
	],
};


/*
Original: #ad66a6, #ad66a6, #742169;
Autumn: #ca9e67, #936037, #683c11;
Spring: #ea527d, #bf3e5e, #872b42;
Summer: #f9b233, #ef7d00, #e94e1b;
Winter: #50c3f1, #0379bd, #075188;
 */
